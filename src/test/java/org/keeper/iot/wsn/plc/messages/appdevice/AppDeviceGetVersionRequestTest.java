package org.keeper.iot.wsn.plc.messages.appdevice;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;
import static com.esotericsoftware.minlog.Log.info;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.esotericsoftware.minlog.Log;

import org.junit.BeforeClass;
import org.junit.Test;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class AppDeviceGetVersionRequestTest {
  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Log.set(LEVEL_TRACE);
  }

  @Test
  public void testCreateMessage() {
    InetAddress address = null;
    try {
      address = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    byte endpoint = (byte) 127;
    Message msg = AppDeviceGetVersionRequest.createMessage(address, endpoint);

    assertTrue(MessageDebugHelpers.isSameAddresses("test", address, msg.getAddress()));
    byte[] msgData = msg.getRaw();

    int len = Helpers.getUnsigned16FromArrayLittleEndian(msgData, 0);
    assertEquals(7, len);
    assertEquals(msgData[2], MessageFields.SOF);
    assertEquals(msgData[3], MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_APP);
    assertEquals(msgData[4], MessageFields.APP_CLUSTER_DEVICE);
    assertEquals(msgData[5], AppDevice.DEV_CMD_GET_VERSION);
    assertEquals(MessageFields.getEndpoint(msgData), 127);

    info("UNIT_TEST", "Message toString:" + msg.toString());
  }

}
