package org.keeper.iot.wsn.plc.resources;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.appdevice.AppDevice;
import org.keeper.iot.wsn.plc.observer.IObserver;

import com.esotericsoftware.minlog.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class AppDeviceResourceManagerTest {
  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Log.set(LEVEL_TRACE);
  }
  
  class CustomCore implements ICore {
    Message lastMessageSent = null;

    public CustomCore() {}

    @Override
    public void subscribe(IObserver obs, InetAddress addr, int msgId) {}

    @Override
    public void subscribe(IObserver obs, int msgId) {}

    @Override
    public void unsubscribe(IObserver obs, InetAddress addr, int msgId) {}

    @Override
    public void unsubscribe(IObserver obs, int msgId) {}

    @Override
    public void send(Message msg) {
      lastMessageSent = msg;
    }

    @Override
    public void sendAndSubscribeOneShot(IObserver obs, Message msg) {}

  }

  @Test
  public void test() {
    CustomCore core = new CustomCore();

    InetAddress address = null;
    try {
      address = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    byte endpoint = 1;
    IResourceManager rM = new AppDeviceResourceManager(core, address, endpoint);

    assertEquals(address, rM.getAddress());
    PlcResource root = rM.getRoot();
    assertEquals("Device", root.getName());
    assertNull(root.getValue());
    assertNotNull(root.getChild(endpoint, MessageFields.APP_CLUSTER_DEVICE,
        AppDevice.DEV_ATT_CUSTOM_ID));
    String[] path = {"Device", "ID"};
    assertNotNull(root.getChild(path, 0));
    
    assertNull(rM.getChild(""));
    assertNotNull(rM.getChild("Device\\ID"));
  }

}
