/**
 * 
 */
package org.keeper.iot.wsn.plc.router;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;
import static com.esotericsoftware.minlog.Log.trace;

import static org.junit.Assert.*;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.keeper.iot.wsn.plc.core.Configurables;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.dummy.DummyRequest;
import org.keeper.iot.wsn.plc.messages.dummy.DummyResponseMessage;
import org.keeper.udp.IDatagramSender;
import org.keeper.utils.Helpers;

import com.esotericsoftware.minlog.Log;

/**
 * @author keeper
 *
 */
public class PlcMessageRouterTest {
  class TestPlcMessageListener implements IPlcMessageListener {
    Message lastPlcMessage = null;
    Message lastPlcTimeoutMessage = null;

    @Override
    public void onPlcMessage(Message msg) {
      lastPlcMessage = msg;
    }

    @Override
    public void onPlcMessageTimeout(Message msg) {
      lastPlcTimeoutMessage = msg;
    }

  }

  class TestDatagramSender implements IDatagramSender {
    private static final String LOG_TAG = "TestDatagramSender";
    DatagramPacket lastDatagramSent = null;

    @Override
    public void sendPacket(DatagramPacket packet) {
      trace(LOG_TAG, "sending");
      lastDatagramSent = packet;
    }

  }

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Log.set(LEVEL_TRACE);
    Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS = 100; // !!!
  }

  /**
   * Test method for
   * {@link org.keeper.iot.wsn.plc.router.PlcMessageRouter#PlcMessageRouter(org.keeper.iot.wsn.plc.router.IPlcMessageListener)}
   * .
   */
  @Test
  public void testPlcMessageRouter() {
    TestPlcMessageListener listener = new TestPlcMessageListener();
    PlcMessageRouter router = new PlcMessageRouter(listener);
    assertNotNull(router);
  }

  /**
   * Test method for
   * {@link org.keeper.iot.wsn.plc.router.PlcMessageRouter#PlcMessageRouter(org.keeper.iot.wsn.plc.router.IPlcMessageListener)}
   * .
   */
  @Test(expected = java.lang.IllegalArgumentException.class)
  public void testPlcMessageRouterNull() {
    PlcMessageRouter router = new PlcMessageRouter(null);
    assertNotNull(router);
  }

  /**
   * Test method for
   * {@link org.keeper.iot.wsn.plc.router.PlcMessageRouter#enqueue(org.keeper.iot.wsn.plc.messages.Message)}
   * .
   */
  @Test
  public void testEnqueue() {
    TestPlcMessageListener listener = new TestPlcMessageListener();
    PlcMessageRouter router = new PlcMessageRouter(listener);
    TestDatagramSender sender = new TestDatagramSender();
    router.setDatagramSender(sender);

    Message msgDev0_0 = null;
    try {
      msgDev0_0 = DummyRequest.createMessage(InetAddress.getByName("192.168.1.100"));
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    router.enqueue(msgDev0_0);
    assertTrue(Helpers.compareArrays(msgDev0_0.getRaw(), sender.lastDatagramSent.getData()));
    assertTrue(MessageDebugHelpers.isSameAddresses("test", msgDev0_0.getAddress(),
        sender.lastDatagramSent.getAddress()));
  }

  /**
   * Test method for
   * {@link org.keeper.iot.wsn.plc.router.PlcMessageRouter#onDatagram(java.net.DatagramPacket)}.
   */
  @Test
  public void testOnDatagram() {
    TestPlcMessageListener listener = new TestPlcMessageListener();
    PlcMessageRouter router = new PlcMessageRouter(listener);
    TestDatagramSender sender = new TestDatagramSender();
    router.setDatagramSender(sender);

    router.onDatagram(new DatagramPacket(DummyResponseMessage.DATA,
        DummyResponseMessage.DATA.length, DummyResponseMessage.dummyAddress, 1234));

    Message dummy = DummyResponseMessage.create(DummyResponseMessage.dummyAddress);

    assertEquals(dummy, listener.lastPlcMessage);
  }

  /**
   * Test method for
   * {@link org.keeper.iot.wsn.plc.router.PlcMessageRouter#onTimeout(org.keeper.iot.wsn.plc.messages.Message)}
   * .
   */
  @Test
  public void testOnTimeout() {
    TestPlcMessageListener listener = new TestPlcMessageListener();
    PlcMessageRouter router = new PlcMessageRouter(listener);
    TestDatagramSender sender = new TestDatagramSender();
    router.setDatagramSender(sender);

    Message msgDev0_0 = null;
    try {
      msgDev0_0 = DummyRequest.createMessage(InetAddress.getByName("192.168.1.100"));
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    router.enqueue(msgDev0_0);

    assertTrue(Helpers.compareArrays(msgDev0_0.getRaw(), sender.lastDatagramSent.getData()));
    assertTrue(MessageDebugHelpers.isSameAddresses("test", msgDev0_0.getAddress(),
        sender.lastDatagramSent.getAddress()));

    try {
      Thread.sleep(Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS + 10);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    assertEquals(msgDev0_0, listener.lastPlcTimeoutMessage);
  }
}
