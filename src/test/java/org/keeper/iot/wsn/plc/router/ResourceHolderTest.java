/**
 * 
 */
package org.keeper.iot.wsn.plc.router;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;
import static com.esotericsoftware.minlog.Log.trace;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import com.esotericsoftware.minlog.Log;

import org.junit.BeforeClass;
import org.junit.Test;
import org.keeper.iot.wsn.plc.core.Configurables;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.dummy.DummyRequest;
import org.keeper.iot.wsn.plc.messages.dummy.DummyResponseMessage;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * @author keeper
 *
 */
public class ResourceHolderTest {
  class TestRouter implements IRouter {
    private static final String LOG_TAG = "TestRouter";
    public Message lastMessageSent = null;
    public Message lastTimeoutMessage = null;

    @Override
    public void enqueue(Message msg) {
      fail("ResourceHolder doesn't call this");
    }

    @Override
    public void send(Message msg) {
      trace(LOG_TAG, "sending");
      lastMessageSent = msg;
    }

    @Override
    public void onTimeout(Message msg) {
      trace(LOG_TAG, "timeout");
      lastTimeoutMessage = msg;
    }
  }

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS = 300; // !!! Inconsistent test results
    // in eclipse executing junit works but running in gradle sometimes fails.
    Log.set(LEVEL_TRACE);
  }

  /**
   * Test method for
   * {@link org.keeper.iot.wsn.plc.router.ResourceHolder#ResourceHolder(org.keeper.iot.wsn.plc.router.IRouter)}
   * .
   */
  @Test
  public void testResourceHolder() {
    TestRouter router = new TestRouter();
    ResourceHolder rHolder = new ResourceHolder(router);
    assertNotNull(rHolder);
  }

  /**
   * Test method for
   * {@link org.keeper.iot.wsn.plc.router.ResourceHolder#ResourceHolder(org.keeper.iot.wsn.plc.router.IRouter)}
   * .
   */
  @Test(expected = java.lang.IllegalArgumentException.class)
  public void testResourceHolderNull() {
    ResourceHolder rHolder = new ResourceHolder(null);
    assertNotNull(rHolder);
  }

  /**
   * Test method for
   * {@link org.keeper.iot.wsn.plc.router.ResourceHolder#addMessage(org.keeper.iot.wsn.plc.messages.Message)}
   * .
   */
  @Test
  public void testAddMessage() {
    TestRouter router = new TestRouter();
    ResourceHolder rHolder = new ResourceHolder(router);

    Message msgDev0_0 = null;
    Message msgDev0_1 = null;
    Message msgDev0_2 = null;
    Message msgDev0_3 = null;
    Message msgDev1_0 = null;
    Message msgDev2_0 = null;
    try {
      msgDev0_0 = DummyRequest.createMessage(InetAddress.getByName("192.168.1.100"));
      msgDev0_1 = DummyRequest.createMessage(InetAddress.getByName("192.168.1.100"));
      msgDev0_2 = DummyRequest.createMessage(InetAddress.getByName("192.168.1.100"));
      msgDev0_3 = DummyRequest.createMessage(InetAddress.getByName("192.168.1.100"));
      msgDev1_0 = DummyRequest.createMessage(InetAddress.getByName("192.168.1.101"));
      msgDev2_0 = DummyRequest.createMessage(InetAddress.getByName("192.168.1.102"));
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    rHolder.addMessage(msgDev0_0);

    assertEquals(msgDev0_0, router.lastMessageSent);
    router.lastMessageSent = null;

    /**
     * Wait for packet to timeout because it doesn't receive response, depends on
     * {@link org.keeper.iot.wsn.plc.router.ResourceHolder#Resource.MILLISECS_TIMEOUT}.
     */
    try {
      Thread.sleep(Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS
          + Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS / 4);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    assertEquals(null, router.lastMessageSent);
    assertEquals(msgDev0_0, router.lastTimeoutMessage);

    /**
     * Add a couple of messages to same target device, let them timeout.
     */
    router.lastMessageSent = null;
    router.lastTimeoutMessage = null;
    rHolder.addMessage(msgDev0_0);
    rHolder.addMessage(msgDev0_1);
    rHolder.addMessage(msgDev0_2);
    rHolder.addMessage(msgDev0_3);
    assertEquals(msgDev0_0, router.lastMessageSent);
    assertEquals(null, router.lastTimeoutMessage);
    try {
      Thread.sleep(Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS
          + Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS / 4);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }
    assertEquals(msgDev0_1, router.lastMessageSent);
    assertEquals(msgDev0_0, router.lastTimeoutMessage);
    try {
      Thread.sleep(Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }
    assertEquals(msgDev0_2, router.lastMessageSent);
    assertEquals(msgDev0_1, router.lastTimeoutMessage);
    try {
      Thread.sleep(Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }
    assertEquals(msgDev0_3, router.lastMessageSent);
    assertEquals(msgDev0_2, router.lastTimeoutMessage);
    try {
      Thread.sleep(Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    /**
     * Add messages sent to different devices.
     */
    router.lastMessageSent = null;
    router.lastTimeoutMessage = null;
    rHolder.addMessage(msgDev0_0);
    try {
      Thread.sleep(Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS / 2
          + Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS / 4);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }
    rHolder.addMessage(msgDev0_1);
    assertEquals(msgDev0_0, router.lastMessageSent);
    assertEquals(null, router.lastTimeoutMessage);
    rHolder.addMessage(msgDev1_0);
    assertEquals(msgDev1_0, router.lastMessageSent);
    assertEquals(null, router.lastTimeoutMessage);
    rHolder.addMessage(msgDev2_0);
    assertEquals(msgDev2_0, router.lastMessageSent);
    assertEquals(null, router.lastTimeoutMessage);
    try {
      Thread.sleep(Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS / 2);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }
    assertEquals(msgDev0_1, router.lastMessageSent);
    assertEquals(msgDev0_0, router.lastTimeoutMessage);
    try {
      Thread.sleep(Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS
          + Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS / 4);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }
    assertEquals(msgDev0_1, router.lastMessageSent);
    assertEquals(msgDev0_1, router.lastTimeoutMessage);
  }

  /**
   * Testing concurrency.
   */
  @Test
  public void testConcurrency() {
    TestRouter router = new TestRouter();
    final ResourceHolder rHolder = new ResourceHolder(router);

    final ArrayList<Message> requestMessages = new ArrayList<Message>();
    final ArrayList<Message> responseMessages = new ArrayList<Message>();
    for (int i = 0; i < 5; ++i) {
      try {
        requestMessages.add(DummyRequest.createMessage(InetAddress.getByName("192.168.1.100")));
        responseMessages.add(DummyResponseMessage.create(InetAddress.getByName("192.168.1.100")));
      } catch (UnknownHostException e) {
        e.printStackTrace();
        fail("Should not happen");
      }
    }

    class Enqueuer implements Runnable {
      public boolean keepWorking = true;

      @Override
      public void run() {
        while (keepWorking) {
          int i = Helpers.getRandomInt(0, requestMessages.size() - 1);
          rHolder.addMessage(requestMessages.get(i));
        }
      }
    }

    class Dequeuer implements Runnable {
      public boolean keepWorking = true;

      @Override
      public void run() {
        while (keepWorking) {
          int i = Helpers.getRandomInt(0, responseMessages.size() - 1);
          rHolder.notifyMessageReceived(requestMessages.get(i));
        }
      }
    }

    Enqueuer enqeuer = new Enqueuer();
    Dequeuer dequeuer = new Dequeuer();
    Thread enThread = new Thread(enqeuer, "enqeuer");
    Thread deThread = new Thread(dequeuer, "dequeuer");
    enThread.start();
    deThread.start();

    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    enqeuer.keepWorking = false;
    dequeuer.keepWorking = false;

    try {
      enThread.join();
      deThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }
  }

  /**
   * Test method for
   * {@link org.keeper.iot.wsn.plc.router.ResourceHolder#notifyMessageReceived(org.keeper.iot.wsn.plc.messages.Message)}
   * .
   */
  @Test
  public void testNotifyMessageReceived() {
    TestRouter router = new TestRouter();
    ResourceHolder rHolder = new ResourceHolder(router);

    Message msgDev0_0 = null;
    Message msgDev0_1 = null;
    Message msgDev0_0Rsp = null;
    try {
      msgDev0_0 = DummyRequest.createMessage(InetAddress.getByName("192.168.1.100"));
      msgDev0_1 = DummyRequest.createMessage(InetAddress.getByName("192.168.1.100"));
      msgDev0_0Rsp = DummyResponseMessage.create(InetAddress.getByName("192.168.1.100"));
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    rHolder.addMessage(msgDev0_0);
    rHolder.addMessage(msgDev0_1);
    assertEquals(msgDev0_0, router.lastMessageSent);
    assertEquals(null, router.lastTimeoutMessage);
    rHolder.notifyMessageReceived(msgDev0_0Rsp);
    assertEquals(msgDev0_1, router.lastMessageSent);
    assertEquals(null, router.lastTimeoutMessage);
    try {
      Thread.sleep(Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS
          + Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS / 4);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }
    assertEquals(msgDev0_1, router.lastMessageSent);
    assertEquals(msgDev0_1, router.lastTimeoutMessage);

    /*
     * Do the same but don't let the message timeout occur.
     */
    router.lastMessageSent = null;
    router.lastTimeoutMessage = null;
    rHolder.addMessage(msgDev0_0);
    rHolder.addMessage(msgDev0_1);
    assertEquals(msgDev0_0, router.lastMessageSent);
    assertEquals(null, router.lastTimeoutMessage);
    rHolder.notifyMessageReceived(msgDev0_0Rsp);
    assertEquals(msgDev0_1, router.lastMessageSent);
    assertEquals(null, router.lastTimeoutMessage);
    rHolder.notifyMessageReceived(msgDev0_0Rsp);
    try {
      Thread.sleep(Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS
          + Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS / 4);
    } catch (InterruptedException e) {
      e.printStackTrace();
      fail("Should not happen");
    }
    assertEquals(msgDev0_1, router.lastMessageSent);
    assertEquals(null, router.lastTimeoutMessage);
  }

}
