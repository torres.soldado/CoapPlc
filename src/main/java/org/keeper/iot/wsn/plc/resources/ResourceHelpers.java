package org.keeper.iot.wsn.plc.resources;

import static com.esotericsoftware.minlog.Log.error;

import org.keeper.iot.wsn.plc.messages.DataTypes;
import org.keeper.utils.Helpers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ResourceHelpers {
  public static String resourceToString(byte datatype, byte[] rawData, int datalen) {
    switch (datatype) {
      case DataTypes.DATATYPE_INT8_ID:
        if (rawData.length == DataTypes.DATATYPE_INT8_SIZE) {
          return String.valueOf(rawData[0]);
        }
        break;
      case DataTypes.DATATYPE_UINT8_ID:
        if (rawData.length == DataTypes.DATATYPE_UINT8_SIZE) {
          int tmp = rawData[0] & 0xFF;
          return String.valueOf(tmp);
        }
        break;
      case DataTypes.DATATYPE_INT16_ID:
        if (rawData.length == DataTypes.DATATYPE_INT16_SIZE) {
          return String.valueOf(Helpers.getSigned16FromArrayLittleEndian(rawData, 0));
        }
        break;
      case DataTypes.DATATYPE_UINT16_ID:
        if (rawData.length == DataTypes.DATATYPE_UINT16_SIZE) {
          return String.valueOf(Helpers.getUnsigned16FromArrayLittleEndian(rawData, 0));
        }
        break;
      case DataTypes.DATATYPE_INT32_ID:
        if (rawData.length == DataTypes.DATATYPE_INT32_SIZE) {
          return String.valueOf(Helpers.getSigned32FromArrayLittleEndian(rawData, 0));
        }
        break;
      case DataTypes.DATATYPE_UINT32_ID:
        if (rawData.length == DataTypes.DATATYPE_UINT32_SIZE) {
          return String.valueOf(Helpers.getUnsigned32FromArrayLittleEndian(rawData, 0));
        }
        break;
      case DataTypes.DATATYPE_INT64_ID:
        if (rawData.length == DataTypes.DATATYPE_INT64_SIZE) {
          return String.valueOf(Helpers.getSigned64FromArrayLittleEndian(rawData, 0));
        }
        break;
      case DataTypes.DATATYPE_UINT64_ID:
        if (rawData.length == DataTypes.DATATYPE_UINT64_SIZE) {
          return Long.toUnsignedString(Helpers.getUnsigned64FromArrayLittleEndian(rawData, 0));
        }
        break;
      case DataTypes.DATATYPE_FLOAT_ID:
        if (rawData.length == DataTypes.DATATYPE_FLOAT_SIZE) {
          return String.valueOf(ByteBuffer.wrap(rawData).getFloat());
        }
        break;
      case DataTypes.DATATYPE_DOUBLE_ID:
        if (rawData.length == 8) {
          return String.valueOf(ByteBuffer.wrap(rawData).getDouble());
        }
        break;
      case DataTypes.DATATYPE_BYTE_ARRAY_ID:
        if (rawData.length == datalen) {
          return Helpers.arrayToHexString(rawData, datalen);
//          return new String(rawData);
        }
        break;
    }

    error(
        "resourceToString",
        "Error converting data to String, datatype(" + String.valueOf(datatype)
            + "), rawData.length(" + String.valueOf(rawData.length) + "), datalen("
            + String.valueOf(datalen) + ")");
    return null;
  }

  public static byte[] resourceStringToRaw(byte datatype, String resourceValue, int datalen) {
    byte[] src;
    byte[] ret;
    switch (datatype) {
      case DataTypes.DATATYPE_INT8_ID:
      case DataTypes.DATATYPE_UINT8_ID:
        src =
            ByteBuffer.allocate(Integer.SIZE / Byte.SIZE).order(ByteOrder.LITTLE_ENDIAN)
                .putInt(Integer.decode(resourceValue)).array();
        ret = new byte[DataTypes.DATATYPE_UINT8_SIZE];
        ret[0] = src[0];
        return ret;
      case DataTypes.DATATYPE_INT16_ID:
      case DataTypes.DATATYPE_UINT16_ID:
        src =
            ByteBuffer.allocate(Integer.SIZE / Byte.SIZE).order(ByteOrder.LITTLE_ENDIAN)
                .putInt(Integer.decode(resourceValue)).array();
        ret = new byte[DataTypes.DATATYPE_UINT16_SIZE];
        Helpers.arrayToArray(src, ret, 0, ret.length, 0);
        return ret;
      case DataTypes.DATATYPE_INT32_ID:
      case DataTypes.DATATYPE_UINT32_ID:
        src =
            ByteBuffer.allocate(Long.SIZE / Byte.SIZE).order(ByteOrder.LITTLE_ENDIAN)
                .putLong(Long.decode(resourceValue)).array();
        ret = new byte[DataTypes.DATATYPE_UINT32_SIZE];
        Helpers.arrayToArray(src, ret, 0, ret.length, 0);
        return ret;
      case DataTypes.DATATYPE_INT64_ID:
        return ByteBuffer.allocate(Long.SIZE / Byte.SIZE).order(ByteOrder.LITTLE_ENDIAN)
            .putLong(Long.parseLong(resourceValue)).array();
      case DataTypes.DATATYPE_UINT64_ID:
        return ByteBuffer.allocate(Long.SIZE / Byte.SIZE).order(ByteOrder.LITTLE_ENDIAN)
            .putLong(Long.parseUnsignedLong(resourceValue)).array();
      case DataTypes.DATATYPE_FLOAT_ID:
        return ByteBuffer.allocate(Float.SIZE / Byte.SIZE).putFloat(Float.valueOf(resourceValue))
            .order(ByteOrder.LITTLE_ENDIAN).array();
      case DataTypes.DATATYPE_DOUBLE_ID:
        return ByteBuffer.allocate(Double.SIZE / Byte.SIZE)
            .putDouble(Double.valueOf(resourceValue)).order(ByteOrder.LITTLE_ENDIAN).array();
      case DataTypes.DATATYPE_BYTE_ARRAY_ID:
        ret = new byte[datalen];
        byte[] data = resourceValue.getBytes();
        Helpers.arrayToArray(data, ret, 0, datalen, 0);
        return ret;
    }

    error("resourceToString",
        "Error converting data to String, datatype(" + String.valueOf(datatype)
            + "), resourceValue(\"" + resourceValue + "\"), datalen(" + String.valueOf(datalen)
            + ")");
    return null;
  }
}
