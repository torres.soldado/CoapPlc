package org.keeper.iot.wsn.plc.observer;

/**
 * Holds an observer reference and a flag that indicates if observer should be unregistered after
 * it receives a response/timeout.
 * 
 * @author sergio.soldado@withus.pt
 */
public class ObserverEntry {
  @Override
  protected Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException("Not supported");
  }

  /**
   * Observer.
   */
  public final IObserver obs;

  /**
   * If true must remove this observer after a response or timeout.
   */
  public final boolean removeWhenNotified;

  /**
   * Default constructor not allowed.
   */
  @SuppressWarnings("unused")
  private ObserverEntry() {
    obs = null;
    removeWhenNotified = true;
  }

  /**
   * Constructor for ObserverEntry.
   * 
   * @param obs IObserver
   * @param removeWhenNotified boolean
   */
  public ObserverEntry(IObserver obs, boolean removeWhenNotified) {
    this.obs = obs;
    this.removeWhenNotified = removeWhenNotified;
  }
}
