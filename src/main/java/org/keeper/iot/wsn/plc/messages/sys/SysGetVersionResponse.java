/**
 * SysGetVersionResponse.java
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.sys;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class SysGetVersionResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = SysGetVersionResponse.class.getSimpleName();

  /**
   * Used to register observer against this message Id.
   */
  public static final int REGISTRATION_ID = MessageFields.createId(new byte[] {4, 0,
      MessageFields.SOF, MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_SYS,
      Sys.SYS_CMD_GET_VERSION, 0});

  /**
   * The PLC status bit-fields.
   */
  private final int status;

  /**
   * The version of this cluster.
   */
  private final byte version;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public SysGetVersionResponse(InetAddress address, byte[] data) throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 5);
    this.version = data[9];
  }

  /**
   * Don't allow.
   */
  private SysGetVersionResponse() {
    super(null, null);
    this.status = 0;
    this.version = 0;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof Sys) {
      ((Sys) obs).onSysGetVersionResponse(super.getAddress(), this.status, this.version);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return LOG_TAG + ": "
        + MessageDebugHelpers.printStatus(this.status)
        + " Version(" + Helpers.byteToHexString(this.version) + ") "
        + super.toString();
  }
}
