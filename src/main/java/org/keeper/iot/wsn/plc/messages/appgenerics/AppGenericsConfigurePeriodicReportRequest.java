/**
 * Creates a get version request {@link Message}.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appgenerics;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.SequenceNumber;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppGenericsConfigurePeriodicReportRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppGenericsConfigurePeriodicReportRequest.class
      .getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * @param endpoint Target endpoint.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address, byte endpoint, byte clusterId,
      byte attributeId, byte attributeDatatype, byte attributeLength, int period) {
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_APP,
            MessageFields.APP_CLUSTER_GENERICS, AppGenerics.GEN_CMD_CFG_PERIODIC_REPORT, endpoint,
            1, clusterId, attributeId, attributeDatatype, attributeLength, 0, 0,
            (byte) SequenceNumber.getSequenceNumber(), 0};

    Helpers.setInt16InArrayLittleEndian(period, messageData, 12);
    Message message =
        new AppGenericsConfigurePeriodicReportRequest(address, messageData,
            AppGenericsConfigurePeriodicReportResponse.getRegistrationId(endpoint));

    return message;
  }

  public byte getClusterId(byte[] data) {
    return data[8];
  }

  public byte getAttributeId(byte[] data) {
    return data[9];
  }

  public byte getAttributeDatatype(byte[] data) {
    return data[10];
  }

  public byte getAttributeDatalen(byte[] data) {
    return data[11];
  }

  public byte getReportPeriod(byte[] data) {
    return data[12];
  }

  /**
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private AppGenericsConfigurePeriodicReportRequest(InetAddress address, byte[] data,
      int responseId) {
    super(address, data, responseId);
  }

  /**
   * Gets string representation.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  public String toString() {
    byte clusterCmdId = MessageFields.getClusterCmdId(super.data);
    byte clusterId = getClusterId(super.data);
    byte attributeId = getAttributeId(super.data);
    byte attributeDataType = getAttributeDatatype(super.data);
    byte attributeDatalen = getAttributeDatalen(super.data);
    byte reportPeriod = getReportPeriod(super.data);

    return LOG_TAG + ": " + super.toString() + ", cmdClusterId(" + Integer.toString(clusterCmdId)
        + "), endpoint(" + Integer.toString(MessageFields.getEndpoint(super.data))
        + "), clusterId(" + Integer.toString(clusterId) + "), attributeId("
        + Integer.toString(attributeId) + "), attributeDatatype("
        + Integer.toString(attributeDataType) + "), attributeDatalen("
        + Integer.toString(attributeDatalen) + "), ReportPeriod(" + Integer.toString(reportPeriod)
        + "), sequenceNumer(" + Integer.toString(MessageFields.getSequenceNumber(super.data)) + ")";
  }
}
