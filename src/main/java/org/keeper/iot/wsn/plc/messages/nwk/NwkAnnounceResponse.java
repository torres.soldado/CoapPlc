/**
 * Creates a network announce response {@link Message} from raw data.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.nwk;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.MessageStatus;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class NwkAnnounceResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = NwkAnnounceResponse.class.getSimpleName();

  /**
   * Used to register observer against this message Id.
   */
  public static final int REGISTRATION_ID = MessageFields
      .createId(new byte[] {0, 0, MessageFields.SOF,
          MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_NWK, Nwk.NWK_ANNOUNCE, 0});

  /**
   * The PLC status bit-fields.
   */
  private final int status;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   */
  public NwkAnnounceResponse(InetAddress address, byte[] data) {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 5);
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof Nwk) {
      ((Nwk) obs).onNwkAnnounceResponse(super.getAddress(), this.status);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  public String toString() {
    return LOG_TAG + ": " + super.toString() + ", Status("
        + MessageStatus.getStatusStr(this.status) + ")";
  }

}
