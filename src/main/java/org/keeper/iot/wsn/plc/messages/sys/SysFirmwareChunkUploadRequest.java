/**
 * SysFirmwareChunkUploadRequest.java
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.sys;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * Implements the message builder for SysFirmwareChunkUploadRequest.
 * 
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class SysFirmwareChunkUploadRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = SysFirmwareChunkUploadRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * @param chunk Segment of data from raw firmware binary image along with meta data.
   * 
   * @return Message.
   * 
   */
  public static Message createMessage(InetAddress address, FirmwareDataChunk chunk) {
    if (chunk.targetModel.length != SysAttributes.MODEL_LEN) {
      throw new IllegalArgumentException("bad length for model");
    }
    if (chunk.targetHardwareRevision.length != SysAttributes.HW_VER_LEN) {
      throw new IllegalArgumentException("bad length for hardware revision");
    }
    if (chunk.targetFirmwareVersion.length != SysAttributes.FW_VER_LEN) {
      throw new IllegalArgumentException("bad length for firmware version");
    }
    if (chunk.chunkData.length < 1 || chunk.chunkData.length > SysAttributes.FW_MAX_CHUNK_SIZE) {
      throw new IllegalArgumentException("bad firmware chunk size");
    }

    byte[] data =
        new byte[5 + SysAttributes.MODEL_LEN + SysAttributes.HW_VER_LEN
            + SysAttributes.FW_VER_LEN + 2 + 2 + 4 + 2 + chunk.chunkData.length + 1];

    MessageFields.setCmd0(data, (byte) (MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_SYS));
    MessageFields.setCmd1AkaClusterId(data, Sys.SYS_CMD_FW_UPLOAD_CHUNK);
    Helpers.arrayToArray(chunk.targetModel, data, 0, SysAttributes.MODEL_LEN, 5);
    Helpers.arrayToArray(chunk.targetHardwareRevision, data, 0, SysAttributes.HW_VER_LEN,
        5 + SysAttributes.MODEL_LEN);
    Helpers.arrayToArray(chunk.targetFirmwareVersion, data, 0, SysAttributes.FW_VER_LEN, 5
        + SysAttributes.MODEL_LEN + SysAttributes.HW_VER_LEN);
    Helpers.setInt16InArrayLittleEndian(chunk.chunkNumber, data, 5 + SysAttributes.MODEL_LEN
        + SysAttributes.HW_VER_LEN + SysAttributes.FW_VER_LEN);
    Helpers.setInt16InArrayLittleEndian(chunk.totalNumberOfChunks, data, 5
        + SysAttributes.MODEL_LEN + SysAttributes.HW_VER_LEN + SysAttributes.FW_VER_LEN
        + 2);
    Helpers.setInt32InArrayLittleEndian(chunk.chunkOffset, data, 5 + SysAttributes.MODEL_LEN
        + SysAttributes.HW_VER_LEN + SysAttributes.FW_VER_LEN + 4);
    Helpers.setInt16InArrayLittleEndian(chunk.chunkData.length, data, 5
        + SysAttributes.MODEL_LEN + SysAttributes.HW_VER_LEN + SysAttributes.FW_VER_LEN
        + 8);
    Helpers.arrayToArray(chunk.chunkData, data, 0, chunk.chunkData.length, 5
        + SysAttributes.MODEL_LEN + SysAttributes.HW_VER_LEN + SysAttributes.FW_VER_LEN
        + 10);

    Message message = new SysFirmwareChunkUploadRequest(address, data);

    return message;
  }

  /**
   * SysFirmwareChunkUploadRequest.
   * 
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private SysFirmwareChunkUploadRequest(InetAddress address, byte[] data) {
    super(address, data, SysFirmwareChunkUploadResponse.REGISTRATION_ID, 30000);
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  public String toString() {
    return LOG_TAG + super.toString();
  }
}
