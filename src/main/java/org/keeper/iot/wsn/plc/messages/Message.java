/**
 * Message.java
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages;

import static com.esotericsoftware.minlog.Log.trace;
import static com.esotericsoftware.minlog.Log.warn;

import org.keeper.iot.wsn.plc.core.Configurables;
import org.keeper.iot.wsn.plc.messages.nwk.Nwk;
import org.keeper.iot.wsn.plc.messages.nwk.NwkAnnounceResponse;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.util.Arrays;

/**
 * Plc Message.
 * 
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class Message {
  /**
   * Just a tag for logger.
   */
  private static final String LOG_TAG = Message.class.getSimpleName();

  /**
   * Address of origin/destination device.
   */
  protected final InetAddress address;

  /**
   * Message payload data.
   */
  protected final byte[] data;

  /**
   * A "special" id used to identify a message efficiently.
   */
  private final int id;

  /**
   * The id of the response to this message, if applicable.
   */
  private final int responseId;

  /**
   * Timeout for message response.
   */
  private final int timeoutMilliseconds;

  /**
   * Gets message timeout field.
   * 
   * @return int Timeout value in milliseconds.
   */
  public int getResponseTimeout() {
    return this.timeoutMilliseconds;
  }

  private byte[] initialiseLocalMessageData(byte[] externalData) {
    byte[] localData;

    if (externalData[0] == 0 && externalData[1] == 0) {
      // Assume message is being created and these fields were omitted on purpose.
      localData = Arrays.copyOf(externalData, externalData.length);
      MessageFields.setLenAuto(localData);
      MessageFields.setSof(localData);
      MessageFields.setChecksumAuto(localData);
    } else {
      int length = MessageFields.getLength(externalData);
      localData = Arrays.copyOf(externalData, length + 2);
    }

    return localData;
  }

  /**
   * Construct a message from address and raw data.
   * If the first 2 bytes of the message are zero the length and checksum will be filled in
   * automatically.
   * 
   * @param address Address of source/destination.
   * @param data Raw message data.
   * 
   * @throws IllegalArgumentException Thrown if message is invalid (e.g. wrong checksum).
   */
  public Message(InetAddress address, byte[] data) throws IllegalArgumentException {
    if (null == address) {
      throw new IllegalArgumentException("null address");
    }

    if (null == data) {
      throw new IllegalArgumentException("null data");
    }

    this.data = initialiseLocalMessageData(data);

    if (!isValidMessage()) {
      throw new IllegalArgumentException("Not a valid message");
    }

    this.timeoutMilliseconds = Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS;
    this.responseId = 0;
    this.address = address;
    this.id = MessageFields.createId(this.data);
  }

  /**
   * Construct a message from address and raw data.
   * If the first 2 bytes of the message are zero the length and checksum will be filled in
   * automatically.
   * 
   * @param address Address of source/destination.
   * @param data Raw message data.
   * @param responseId Response message Id.
   * 
   * @throws IllegalArgumentException Thrown if message is invalid (e.g. wrong checksum).
   */
  protected Message(InetAddress address, byte[] data, int responseId)
      throws IllegalArgumentException {
    if (null == address) {
      throw new IllegalArgumentException("null address");
    }

    if (null == data) {
      throw new IllegalArgumentException("null data");
    }

    this.data = initialiseLocalMessageData(data);

    if (!isValidMessage()) {
      throw new IllegalArgumentException("Isn't valid message");
    }

    this.timeoutMilliseconds = Configurables.DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS;
    this.responseId = responseId;
    this.address = address;
    this.id = MessageFields.createId(this.data);
  }

  /**
   * Construct a message from address and raw data.
   * If the first 2 bytes of the message are zero the length and checksum will be filled in
   * automatically.
   * 
   * @param address Address of source/destination.
   * @param data Raw message data.
   * @param responseId Response message Id.
   * @param timeoutMs Timeout value of response message in milliseconds.
   * 
   * @throws IllegalArgumentException Thrown if message is invalid (e.g. wrong checksum).
   */
  protected Message(InetAddress address, byte[] data, int responseId, int timeoutMs)
      throws IllegalArgumentException {
    if (null == address) {
      throw new IllegalArgumentException("null address");
    }

    if (null == data) {
      throw new IllegalArgumentException("null data");
    }

    this.data = initialiseLocalMessageData(data);

    if (!isValidMessage()) {
      throw new IllegalArgumentException("Isn't valid message");
    }

    this.timeoutMilliseconds = timeoutMs;
    this.responseId = responseId;
    this.address = address;
    this.id = MessageFields.createId(this.data);
  }

  /**
   * Not used.
   */
  @SuppressWarnings("unused")
  private Message() {
    this.data = null;
    this.timeoutMilliseconds = 0;
    this.responseId = 0;
    this.address = null;
    this.id = 0;
  }

  /**
   * Method calculateChecksum.
   * 
   * 
   * @return byte
   */
  private byte calculateChecksum() {
    return MessageFields.calculateChecksum(this.data);
  }

  /**
   * Each subclass calls a specific interface method that the object should implement. For instance
   * a given class is handled a {@link NwkAnnounceResponse}, it then calls this method on the
   * message which in turn calls the specific method as defined in {@link Nwk}, in this case it
   * would be o.onOnNwkAnnounceAReq(..).
   * 
   * @param obs Object on which a subclass specific method is to be called.
   */
  public void callHandler(Object obs) {
    warn(LOG_TAG, "Calling default EMPTY handler");
  }

  /**
   * Method equals.
   * 
   * @param obj Object
   * 
   * @return boolean
   */
  @Override
  public final boolean equals(Object obj) {
    final Message other;
    final byte[] otherData;

    if (this == obj) {
      return true;
    }

    if (null == obj) {
      return false;
    }

    if (!(obj instanceof Message)) {
      return false;
    }

    other = (Message) obj;

    if (!this.address.equals(other.getAddress())) {
      return false;
    }

    if (this.id != other.getId()) {
      return false;
    }

    otherData = other.getRaw();

    if (data.length != otherData.length) {
      return false;
    }

    for (int i = 0; i < data.length; ++i) {
      if (this.data[i] != otherData[i]) {
        return false;
      }
    }

    return true;
  }

  /**
   * Method getAddress.
   * 
   * 
   * @return InetAddress
   */
  public InetAddress getAddress() {
    return this.address;
  }

  /**
   * Method getChecksum.
   * 
   * 
   * @return byte
   */
  public byte getChecksum() {
    return this.data[getLength() + 1];
  }

  /**
   * Method getId.
   * 
   * 
   * @return int
   */
  public int getId() {
    return this.id;
  }

  /**
   * Method getLength.
   * Data might be padded but this returns the "real" plc message payload.
   * 
   * @return int Length of plc message payload.
   */
  public int getLength() {
    return (this.data[0] & 0xFF) | ((this.data[1] & 0xFF) << 8);
  }

  /**
   * Return message payload.
   * 
   * @return byte[]
   */
  public byte[] getRaw() {
    return this.data;
  }

  /**
   * Method getResponseId.
   * 
   * 
   * @return int
   */
  public int getResponseId() {
    return this.responseId;
  }

  /**
   * Method hashCode.
   * 
   * @return int
   */
  @Override
  public int hashCode() {
    return this.id;
  }

  /**
   * Method isValidMessage.
   * 
   * @return boolean
   */
  private boolean isValidMessage() { // TODO This is quite messy. Probably the best solution is to
                                     // move this to MessageFieldsm, on message construction create
                                     // all local default fields (or rely on functions to get them).
    final byte checksum;
    final byte localChecksum;

    if (this.data.length < 5) {
      return false;
    }

    if (this.data[2] != MessageFields.SOF) {
      return false;
    }

    if (getLength() < MessageFields.MIN_MESSAGE_LENGTH
        || getLength() > MessageFields.MAX_MESSAGE_LENGTH) {
      trace(LOG_TAG, "invalid message length");
      return false;
    }

    checksum = getChecksum();
    localChecksum = calculateChecksum();

    if (checksum != localChecksum) {
      trace(LOG_TAG, "checksum mismatch, calculated 0x" + Helpers.byteToHexString(localChecksum)
          + " message has 0x" + Helpers.byteToHexString(checksum));
      return false;
    }

    return true;
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return MessageDebugHelpers.printCmd0(this.data) + MessageDebugHelpers.printCmd1(this.data)
        + MessageDebugHelpers.printLength(getLength())
        + MessageDebugHelpers.printChecksum(getChecksum())
        + MessageDebugHelpers.printMessageId(this.id) + "Response"
        + MessageDebugHelpers.printMessageId(this.responseId);
    // + MessageDebugHelpers.printRawData(this.data, getLength() + 2);
  }
}
