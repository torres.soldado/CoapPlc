package org.keeper.iot.wsn.plc.messages.dbg;

import java.net.InetAddress;

public interface Dbg {
  public static final byte DBG_GET_VERSION = (0);
  
  /**
   * Response with subsystem version.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param version Version of this subsystem.
   */
  public void onDbgGetVersionResponse(InetAddress address, int status, byte version);
}
