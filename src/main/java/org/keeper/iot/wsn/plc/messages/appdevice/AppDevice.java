package org.keeper.iot.wsn.plc.messages.appdevice;

import org.keeper.iot.wsn.plc.messages.DataTypes;

import java.net.InetAddress;

public interface AppDevice {
  /**
   * Command id to get version of cluster.
   */
  public static final byte DEV_CMD_GET_VERSION = 0;

  /**
   * Attribute id for device custom ID.
   */
  public static final byte DEV_ATT_CUSTOM_ID = 0;

  public static final byte DEV_ATT_CUSTOM_ID_DATATYPE = DataTypes.DATATYPE_BYTE_ARRAY_ID;
  public static final byte DEV_ATT_CUSTOM_ID_LENGTH = 20;

  /**
   * Response with cluster version if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param endpoint Endpoint address that originated this message.s
   * @param status Plc status code bitfields.
   * @param version Version of this cluster.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppDeviceGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber);
}
