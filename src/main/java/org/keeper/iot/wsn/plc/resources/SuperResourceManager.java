package org.keeper.iot.wsn.plc.resources;

import static com.esotericsoftware.minlog.Log.*;

import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics.Capabilities;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics.Cluster;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics.Endpoint;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import javax.naming.directory.InvalidAttributesException;

public class SuperResourceManager implements IResourceManager {
  /**
   * Used by logger.
   */
  private final String tag;
  /**
   * Device's network address
   */
  private final InetAddress address;

  private final PlcResource root;

  private final List<IResourceManager> handlers = new ArrayList<IResourceManager>();

  public SuperResourceManager(Capabilities capabilities, ICore core, InetAddress address)
      throws InvalidAttributesException {
    tag = getClass().getSimpleName() + "@" + address.getHostName();
    this.address = address;
    root = new PlcResource(address.getHostAddress());

    for (Byte subsystem : capabilities.subsystems) {
      IResourceManager handler = null;
      switch (subsystem) {
        case MessageFields.SUBSYSTEM_SYS:
          handler = new SystemResourceManager(core, address);
          break;
        case MessageFields.SUBSYSTEM_NWK:
          handler = new NetworkResourceManager(core, address);
          break;
        case MessageFields.SUBSYSTEM_APP:
          break;
        default:
          warn(tag, "Unknown subsystem " + String.valueOf(subsystem));
          break;
      }
      if (null != handler) {
        handlers.add(handler);
        root.addChild(handler.getRoot());
      }
    }

    for (Endpoint endpoint : capabilities.endpoints) {
      trace(tag, "Parsing endpoint: " + String.valueOf(endpoint.id) + " " + endpoint);
      PlcResource child = new PlcResource("Endpoint" + String.valueOf(endpoint.id));
      root.addChild(child);

      for (Cluster cluster : endpoint.clusters) {
        IResourceManager handler = null;
        switch (cluster.id) {
          case MessageFields.APP_CLUSTER_DEVICE:
            handler = new AppDeviceResourceManager(core, address, endpoint.id);
            break;
          case MessageFields.APP_CLUSTER_ONOFF:
            handler = new AppOnOffResourceManager(core, address, endpoint.id);
            break;
          case MessageFields.APP_CLUSTER_SMARTENERGY:
            handler = new AppSmartEnergyResourceManager(core, address, endpoint.id, cluster);
            break;
          case MessageFields.APP_CLUSTER_IDENTIFY:
            handler = new AppIdentifyResourceManager(core, address, endpoint.id);
            break;
          case MessageFields.APP_CLUSTER_SMARTTEMP:
            handler = new AppSmartTempResourceManager(core, address, endpoint.id);
            break;
          default:
            warn(tag, "Unknown cluster id " + String.valueOf(cluster.id));
            break;
        }

        if (null != handler) {
          handlers.add(handler);
          child.addChild(handler.getRoot());
        }
      }
    }
    
    trace(tag, root.toString());
  }

  @Override
  public InetAddress getAddress() {
    return address;
  }

  @Override
  public PlcResource getChild(String[] path) {
    return root.getChild(path, 0);
  }

  @Override
  public PlcResource getRoot() {
    return root;
  }

  @Override
  public void remoteSetAsync(String path, List<String> parameters) {
    MessageDebugHelpers.setResource(tag, path, parameters, root);
  }

  @Override
  public void remoteUpdateAll() {
    root.remoteGetAll();
  }

}
