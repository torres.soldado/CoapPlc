/**
 * Creates a get version request {@link Message}.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appgenerics;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.SequenceNumber;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppGenericsGetCapabilitiesRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppGenericsGetCapabilitiesRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * @param endpoint Target endpoint.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address) {
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_APP,
            MessageFields.APP_CLUSTER_GENERICS, AppGenerics.GEN_CMD_GET_CAPABILITIES, 0,
            (byte) SequenceNumber.getSequenceNumber(), 0};

    Message message =
        new AppGenericsGetCapabilitiesRequest(address, messageData,
            AppGenericsGetCapabilitiesResponse.REGISTRATION_ID);

    return message;
  }

  /**
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private AppGenericsGetCapabilitiesRequest(InetAddress address, byte[] data, int responseId) {
    super(address, data, responseId);
  }

  /**
   * Returns a string representation of message fields.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  public String toString() {
    return LOG_TAG + ": "
        + MessageDebugHelpers.printCmdClusterId(super.data)
        + MessageDebugHelpers.printSequenceNumber(super.data)
        + super.toString();
  }
}
