/**
 * Creates a get version request {@link Message}.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appgenerics;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.SequenceNumber;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppGenericsConfigureRangeAlertRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppGenericsConfigureRangeAlertRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * @param endpoint Target endpoint.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address, byte endpoint, byte clusterId,
      byte attributeId, byte attributeDatatype, byte attributeLength, boolean onChange,
      byte[] infLimit, byte[] supLimit) {

    byte[] messageData = new byte[15 + infLimit.length + supLimit.length];
    MessageFields.setCmd0(messageData,
        (byte) (MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_APP));
    MessageFields.setCmd1AkaClusterId(messageData, MessageFields.APP_CLUSTER_GENERICS);
    MessageFields.setClusterCmdId(messageData, AppGenerics.GEN_CMD_CFG_RANGE_ALERT);
    MessageFields.setEndpoint(messageData, endpoint);
    setAttNum(messageData, (byte) 1);
    setClusterId(messageData, clusterId);
    setAttributeId(messageData, attributeId);
    setOnChange(messageData, onChange);
    setAttributeDatatype(messageData, attributeDatatype);
    setAttributeDatalen(messageData, (byte) attributeLength);
    setSeqNum(messageData, SequenceNumber.getSequenceNumber());
    Helpers.arrayToArray(infLimit, messageData, 0, infLimit.length, 14);
    Helpers.arrayToArray(supLimit, messageData, 0, supLimit.length, 14 + infLimit.length);

    Message message =
        new AppGenericsConfigureRangeAlertRequest(address, messageData,
            AppGenericsConfigureRangeAlertResponse.getRegistrationId(endpoint));

    return message;
  }

  /**
   * Sets the number of attributtes to be configures, always 1.
   * 
   * @param data Message raw data.
   * @param num Number of attributes.
   */
  public static void setAttNum(byte[] data, byte num) {
    data[7] = num;
  }

  /**
   * Sets the cluster id field in raw message data.
   * 
   * @param data Message raw data.
   * @param clusterId Cluseter id.
   */
  public static void setClusterId(byte[] data, byte clusterId) {
    data[8] = clusterId;
  }

  /**
   * Returns the cluster id from the raw message data.
   * 
   * @param data Raw message data.
   * @return Cluster id.
   */
  public static byte getClusterId(byte[] data) {
    return data[8];
  }

  /**
   * Sets the attribute id in the raw message data.
   * 
   * @param data Raw message data.
   * @param attributeId Attribute id.
   */
  public static void setAttributeId(byte[] data, byte attributeId) {
    data[9] = attributeId;
  }

  /**
   * Gets the attrbute id from the raw message data.
   * 
   * @param data Raw message data.
   * @return Attribute id.
   */
  public static byte getAttributeId(byte[] data) {
    return data[9];
  }

  /**
   * Sets the on change field.
   * 
   * @param data Raw message data.
   * @param onChange True if onchange, false otherwise.
   */
  public static void setOnChange(byte[] data, boolean onChange) {
    data[10] =
        (byte) (onChange ? AppRangeAlert.RANGE_ALERT_ONCHANGE_ENABLE
            : AppRangeAlert.RANGE_ALERT_ONCHANGE_DISABLE);
  }

  /**
   * Returns the on change field.
   * 
   * @param data Raw message data.
   * @return true if onChange, false otherwise.
   */
  public static boolean getOnChange(byte[] data) {
    return data[10] != AppRangeAlert.RANGE_ALERT_ONCHANGE_DISABLE;
  }

  /**
   * Sets the attribute data type id in message taw data.
   * 
   * @param data Message rtaw data.
   * @param attributeDatatype Attribute datatype.
   */
  public static void setAttributeDatatype(byte[] data, byte attributeDatatype) {
    data[11] = attributeDatatype;
  }

  /**
   * Gets the attribute data type id from the raw message data.
   * 
   * @param data Raw message data.
   * @return Attribute data type.
   */
  public static byte getAttributeDatatype(byte[] data) {
    return data[11];
  }

  /**
   * Sets the attribute data length field in message raw data.
   * 
   * @param data Message raw data.
   * @param attributeDatalen Attribte data length field.
   */
  public static void setAttributeDatalen(byte[] data, byte attributeDatalen) {
    data[12] = attributeDatalen;
  }

  /**
   * Gets the attribute data length from message raw data.
   * 
   * @param data Messsage raw data.
   * @return Attribute data length.
   */
  public static byte getAttributeDatalen(byte[] data) {
    return data[12];
  }

  /**
   * Sets the sequence number in the rtaw messgae data. Because it divergers from default.
   * 
   * @param data Raw mesgage data.
   * @param seqNum Sequence Number.
   */
  public static void setSeqNum(byte[] data, byte seqNum) {
    data[13] = seqNum;
  }

  /**
   * Gets the sequence number in the raw message data because it differs from the default.
   * 
   * @param data Raw messgae data.
   * @return Sequence numbee.
   */
  public static byte getSeqNum(byte[] data) {
    return data[13];
  }

  /**
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private AppGenericsConfigureRangeAlertRequest(InetAddress address, byte[] data, int responseId) {
    super(address, data, responseId);
  }

  /**
   * String representation.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  public String toString() {
    byte clusterCmdId = MessageFields.getClusterCmdId(super.data);
    byte clusterId = getClusterId(super.data);
    byte attributeId = getAttributeId(super.data);
    byte attributeDataType = getAttributeDatatype(super.data);
    byte attributeDatalen = getAttributeDatalen(super.data);
    boolean onChange = getOnChange(super.data);
    byte[] infLimit = new byte[(super.data.length - 15) / 2];
    Helpers.arrayToArray(data, infLimit, 14, infLimit.length, 0);
    byte[] supLimit = new byte[(super.data.length - 15) / 2];
    Helpers.arrayToArray(data, supLimit, 14 + infLimit.length, supLimit.length, 0);

    return LOG_TAG + ": " + super.toString() + ", cmdClusterId(" + Integer.toString(clusterCmdId)
        + "), endpoint(" + Integer.toString(MessageFields.getEndpoint(super.data))
        + "), clusterId(" + Integer.toString(clusterId) + "), attributeId("
        + Integer.toString(attributeId) + "), attributeDatatype("
        + Integer.toString(attributeDataType) + "), attributeDatalen("
        + Integer.toString(attributeDatalen) + "), onChange(" + Boolean.toString(onChange)
        + "), infLimit(" + Helpers.arrayToHexString(infLimit, infLimit.length) + "), supLimit("
        + Helpers.arrayToHexString(supLimit, supLimit.length) + "), sequenceNumer("
        + Integer.toString(MessageFields.getSequenceNumber(super.data)) + ")";
  }
}
