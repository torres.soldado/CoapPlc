package org.keeper.iot.wsn.plc.resources;

import static com.esotericsoftware.minlog.Log.trace;
import static com.esotericsoftware.minlog.Log.warn;

import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageStatus;
import org.keeper.iot.wsn.plc.messages.sys.*;
import org.keeper.iot.wsn.plc.observer.IObserver;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.util.Date;
import java.util.List;

public class SystemResourceManager implements IResourceManager, IObserver, Sys {
  /**
   * Used by logger.
   */
  private final String TAG;

  /**
   * The target device's address.
   */
  private final InetAddress address;

  private final PlcResource root;
  private final PlcResource resVersion;

  private final PlcResource factoryReset;

  private final PlcResource firmware;
  private final PlcResource firmwareUpload;
  private final PlcResource firmwareUpdate;
  private final PlcResource firmwareVersion;

  private final PlcResource date;

  private final PlcResource hardwareVersion;

  private final PlcResource manufacturerId;

  private final PlcResource modelId;

  private final PlcResource powerSource;

  private final PlcResource serial;

  private final PlcResource temperature;

  private final PlcResource userAction;

  private final PlcResource reboot;

  public SystemResourceManager(ICore core, InetAddress address) {
    this.TAG = this.getClass().getSimpleName() + "@" + address.getHostAddress();
    this.address = address;
    root = new PlcResource("System");

    resVersion = new PlcResource("Version") {
      @Override
      public void remoteGet() {
        core.send(SysGetVersionRequest.createMessage(address));
      }
    };
    core.subscribe(this, address, SysGetVersionResponse.REGISTRATION_ID);
    root.addChild(resVersion);

    factoryReset = new PlcResource("FactoryReset") {
      @Override
      public void remoteSet(List<String> parameters) {
        core.send(SysFactoryResetRequest.createMessage(address));
      }
    };
    core.subscribe(this, SysFactoryResetResponse.REGISTRATION_ID);
    root.addChild(factoryReset);

    firmware = new PlcResource("Firmware");
    root.addChild(firmware);
    firmwareUpload = new PlcResource("Upload", "Upload chunks to this resource") {
      @Override
      public void remoteSet(List<String> parameters) {
        if (null == parameters || 8 != parameters.size()) {
          warn(TAG, "Invalid parameters for " + getName());
          return;
        }

        FirmwareDataChunk chunk = new org.keeper.iot.wsn.plc.messages.sys.FirmwareDataChunk();
        chunk.targetModel =
            ResourceHelpers.resourceStringToRaw(SysAttributes.MODEL_DATATYPE, parameters.get(0),
                SysAttributes.MODEL_LEN);
        chunk.targetHardwareRevision =
            ResourceHelpers.resourceStringToRaw(SysAttributes.HW_VER_DATATYPE, parameters.get(1),
                SysAttributes.HW_VER_LEN);
        chunk.targetFirmwareVersion =
            ResourceHelpers.resourceStringToRaw(SysAttributes.FW_VER_DATATYPE, parameters.get(2),
                SysAttributes.FW_VER_LEN);
        chunk.chunkData =
            ResourceHelpers.resourceStringToRaw(SysAttributes.FW_CHUNK_DATATYPE, parameters.get(3),
                parameters.get(3).length());
        chunk.chunkOffset = Integer.valueOf(parameters.get(4));
        chunk.chunkNumber = Integer.valueOf(parameters.get(5));
        chunk.totalNumberOfChunks = Integer.valueOf(parameters.get(6));

        core.send(SysFirmwareChunkUploadRequest.createMessage(address, chunk));
        trace(
            TAG,
            "Submitted firmware chunk upload:\n" + "targetModel(" + parameters.get(0) + ")\n"
                + "targetHardwareRevision(" + parameters.get(1) + ")\n" + "targetFirmwareVersion("
                + parameters.get(2) + ")\n" + "chunkOffset(" + parameters.get(3) + ")\n"
                + "chunkNumber(" + parameters.get(4) + ")\n" + "totalNumberOfChunks("
                + parameters.get(5) + ")\n" + "chunkData(" + parameters.get(6) + ")\n");
      }
    };
    firmware.addChild(firmwareUpload);
    core.subscribe(this, SysFirmwareChunkUploadResponse.REGISTRATION_ID);

    firmwareUpdate = new PlcResource("Update", "Issue update to current the uploaded image") {
      @Override
      public void remoteSet(List<String> parameters) {
        core.send(SysUpdateFirmwareRequest.createMessage(address));
      }
    };
    core.subscribe(this, address, SysUpdateFirmwareResponse.REGISTRATION_ID);
    firmware.addChild(firmwareUpdate);

    firmwareVersion = new PlcResource("CurrentVersion") {
      @Override
      public void remoteGet() {
        core.send(SysGetFirmwareVersionRequest.createMessage(address));
      }
    };
    core.subscribe(this, address, SysGetFirmwareVersionResponse.REGISTRATION_ID);
    firmware.addChild(firmwareVersion);

    date = new PlcResource("Date") {
      @Override
      public void remoteGet() {
        core.send(SysGetDateRequest.createMessage(address));
      }

      @Override
      public void remoteSet(List<String> parameters) {
        if (null == parameters || 0 == parameters.size()) {
          warn(TAG, "Empty parameters");
          return;
        }

        byte[] date =
            ResourceHelpers.resourceStringToRaw(SysAttributes.DATE_DATATYPE, parameters.get(0),
                SysAttributes.DATE_LEN);
        core.send(SysSetDateRequest.createMessage(address, date));
      }
    };
    core.subscribe(this, address, SysGetDateResponse.REGISTRATION_ID);
    core.subscribe(this, address, SysSetDateResponse.REGISTRATION_ID);
    root.addChild(date);

    hardwareVersion = new PlcResource("HardwareRevision") {
      @Override
      public void remoteGet() {
        core.send(SysGetHardwareRevisionRequest.createMessage(address));
      }
    };
    core.subscribe(this, address, SysGetHardwareRevisionResponse.REGISTRATION_ID);
    root.addChild(hardwareVersion);

    manufacturerId = new PlcResource("ManufacturerId") {
      @Override
      public void remoteGet() {
        core.send(SysGetManufacturerIdRequest.createMessage(address));
      }
    };
    core.subscribe(this, address, SysGetManufacturerIdResponse.REGISTRATION_ID);
    root.addChild(manufacturerId);

    modelId = new PlcResource("ModelId") {
      @Override
      public void remoteGet() {
        core.send(SysGetModelIdRequest.createMessage(address));
      }
    };
    core.subscribe(this, address, SysGetModelIdResponse.REGISTRATION_ID);
    root.addChild(modelId);

    powerSource = new PlcResource("PowerSources") {
      @Override
      public void remoteGet() {
        core.send(SysGetPowersourceRequest.createMessage(address));
      }
    };
    core.subscribe(this, address, SysGetPowersourceResponse.REGISTRATION_ID);
    root.addChild(powerSource);

    serial = new PlcResource("SerialNumber") {
      @Override
      public void remoteGet() {
        core.send(SysGetSerialRequest.createMessage(address));
      }
    };
    core.subscribe(this, address, SysGetSerialResponse.REGISTRATION_ID);
    root.addChild(serial);

    temperature = new PlcResource("CurrentTemperature") {
      @Override
      public void remoteGet() {
        core.send(SysGetTemperatureRequest.createMessage(address));
      }
    };
    core.subscribe(this, address, SysGetTemperatureResponse.REGISTRATION_ID);
    root.addChild(temperature);

    userAction = new PlcResource("UserActions");
    core.subscribe(this, address, SysUserDidSomethingNotification.REGISTRATION_ID);
    root.addChild(userAction);

    reboot = new PlcResource("Reboot") {
      @Override
      public void remoteSet(List<String> parameters) {
        core.send(SysRebootRequest.createMessage(address));
      }
    };
    core.subscribe(this, address, SysRebootResponse.REGISTRATION_ID);
    root.addChild(reboot);
  }

  @Override
  public void onDeviceMessage(Message msg) {
    if (MessageDebugHelpers.isSameAddresses(TAG, this.address, msg.getAddress())) {
      msg.callHandler(this);
    }
  }

  @Override
  public void onDeviceMessageTimeout(int msgId) {
    MessageDebugHelpers.debugOnMessageTimeout(TAG, address, msgId);
  }

  @Override
  public InetAddress getAddress() {
    return address;
  }

  @Override
  public PlcResource getChild(String[] path) {
    return root.getChild(path, 0);
  }

  @Override
  public PlcResource getRoot() {
    return root;
  }

  @Override
  public void remoteSetAsync(String path, List<String> parameters) {
    MessageDebugHelpers.setResource(TAG, path, parameters, root);
  }

  @Override
  public void remoteUpdateAll() {
    root.remoteGetAll();
  }

  @Override
  public void onSysGetVersionResponse(InetAddress address, int status, byte version) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      resVersion.setValue(String.valueOf(version));
    }
  }

  @Override
  public void onSysGetManufacturerIdResponse(InetAddress address, int status, byte[] manufacturerId) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      this.manufacturerId.setValue(new String(manufacturerId));
      // this.manufacturerId.setValue(ResourceHelpers.resourceToString(
      // SysAttributes.MANUF_ID_DATATYPE, manufacturerId, SysAttributes.MANUF_ID_LEN));
    }
  }

  @Override
  public void onSysGetModelIdResponse(InetAddress address, int status, byte[] modelId) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      this.modelId.setValue(new String(modelId));
      // this.modelId.setValue(ResourceHelpers.resourceToString(SysAttributes.MODEL_DATATYPE,
      // modelId,
      // SysAttributes.MODEL_LEN));
    }
  }

  @Override
  public void onSysGetHardwareRevisionResponse(InetAddress address, int status, byte[] hwVersion) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      this.hardwareVersion.setValue("v" + String.valueOf(hwVersion[0]) + "."
          + String.valueOf(hwVersion[1]) + "." + String.valueOf(hwVersion[2]));

      // this.hardwareVersion.setValue(ResourceHelpers.resourceToString(SysAttributes.HW_VER_DATATYPE,
      // hwVersion, SysAttributes.HW_VER_LEN));
    }
  }

  @Override
  public void onSysGetSerialResponse(InetAddress address, int status, byte[] serial) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      this.serial.setValue(ResourceHelpers.resourceToString(SysAttributes.SERIAL_DATATYPE, serial,
          SysAttributes.SERIAL_LEN));
    }
  }

  @Override
  public void onSysGetFirmwareVersionResponse(InetAddress address, int status, byte[] fwVersion) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      this.firmwareVersion.setValue("v" + String.valueOf(fwVersion[0]) + "."
          + String.valueOf(fwVersion[1]) + "." + String.valueOf(fwVersion[2]));
      // this.firmwareVersion.setValue(ResourceHelpers.resourceToString(SysAttributes.FW_VER_DATATYPE,
      // fwVersion, SysAttributes.FW_VER_LEN));
    }
  }

  @Override
  public void onSysGetDateResponse(InetAddress address, int status, byte[] date) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      long epoch = Helpers.getUnsigned32FromArrayLittleEndian(date, 0);
      Date expiry = new Date(epoch * 1000);
      this.date.setValue(expiry.toString());
    }
  }

  @Override
  public void onSysSetDateResponse(InetAddress address, int status) {
    MessageStatus.isStatusSuccessfull(TAG, status);
  }

  @Override
  public void onSysUpdateFirmwareResponse(InetAddress address, int status) {
    MessageStatus.isStatusSuccessfull(TAG, status);
  }

  @Override
  public void onSysRebootResponse(InetAddress address, int status) {
    MessageStatus.isStatusSuccessfull(TAG, status);
  }

  @Override
  public void onSysGetTemperature(InetAddress address, int status, byte temperatureCelsius) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      this.temperature.setValue(String.valueOf(temperatureCelsius) + "°C");
    }
  }

  @Override
  public void onSysGetPowerSource(InetAddress address, int status, byte powerSource) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      this.powerSource.setValue(SysAttributes.getPowerSourceStr(powerSource));
    }
  }

  @Override
  public void onSysFactoryReset(InetAddress address, int status) {
    MessageStatus.isStatusSuccessfull(TAG, status);
  }

  @Override
  public void onSysUserDidSomething(InetAddress address, int status, int actions) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      this.userAction.setValue(SysAttributes.getLocalActionStr(actions) + " at "
          + new Date().toString());
    }
  }

  @Override
  public void onSysFirmwareChunkUpload(InetAddress address, int status) {
    MessageStatus.isStatusSuccessfull(TAG, status);
  }

}
