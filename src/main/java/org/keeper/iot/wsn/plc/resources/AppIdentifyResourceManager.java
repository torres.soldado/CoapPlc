package org.keeper.iot.wsn.plc.resources;

import static com.esotericsoftware.minlog.Log.info;
import static com.esotericsoftware.minlog.Log.warn;

import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.appidentify.AppIdentify;
import org.keeper.iot.wsn.plc.messages.appidentify.AppIdentifyGetStateRequest;
import org.keeper.iot.wsn.plc.messages.appidentify.AppIdentifyGetStateResponse;
import org.keeper.iot.wsn.plc.messages.appidentify.AppIdentifyGetVersionRequest;
import org.keeper.iot.wsn.plc.messages.appidentify.AppIdentifyGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.appidentify.AppidentifySetIdentifyRequest;
import org.keeper.iot.wsn.plc.observer.IObserver;

import java.net.InetAddress;
import java.util.List;

public class AppIdentifyResourceManager implements IResourceManager, IObserver, AppIdentify {
  /**
   * Used by logger.
   */
  private static final String TAG = AppIdentifyResourceManager.class.getSimpleName();

  /**
   * The target device's address.
   */
  private final InetAddress address;

  /**
   * The target endpoint.
   */
  private final byte endpoint;

  private final PlcResource resState;
  private final PlcResource resVersion;
  private final PlcResource root;

  public AppIdentifyResourceManager(final ICore core, final InetAddress address, byte endpoint) {
    this.address = address;
    this.endpoint = endpoint;
    root = new PlcResource("Identify");

    resVersion = new PlcResource("Version") {
      @Override
      public void remoteGet() {
        core.send(AppIdentifyGetVersionRequest.createMessage(address, endpoint));
      }
    };
    core.subscribe(this, address, AppIdentifyGetVersionResponse.getRegistrationId(endpoint));
    root.addChild(resVersion);

    resState = new PlcResource("State") {
      @Override
      public void remoteGet() {
        core.send(AppIdentifyGetStateRequest.createMessage(address, endpoint));
      }

      @Override
      public void remoteSet(List<String> parameters) {
        byte[] durationRaw =
            ResourceHelpers.resourceStringToRaw(AppIdentify.IDENT_ATT_TIMEOUT_DATATYPE,
                parameters.get(0), AppIdentify.IDENT_ATT_TIMEOUT_LENGTH);
        if (durationRaw.length < 0) {
          warn(TAG, "Error setting identify, invalid parameters.");
        }

        core.send(AppidentifySetIdentifyRequest.createMessage(address, endpoint, durationRaw[0]));
        info(TAG,
            "Setting identify mode of" + address.getHostAddress() + " for " + parameters.get(0)
                + "S");
      }
    };
    core.subscribe(this, address, AppIdentifyGetStateResponse.getRegistrationId(endpoint));
    root.addChild(resState);
  }

  @Override
  public InetAddress getAddress() {
    return address;
  }

  @Override
  public PlcResource getChild(String[] path) {
    return root.getChild(path, 0);
  }

  @Override
  public PlcResource getRoot() {
    return root;
  }

  @Override
  public void onAppIdentifyGetStateResponse(InetAddress address, byte endpoint, int status,
      byte state, byte sequenceNumber) {
    if (MessageDebugHelpers.isMessageForMeAndValidStatus(TAG, this.endpoint, endpoint, status)) {
      if (state == AppIdentify.IDENT_OFF) {
        resState.setValue("Off");
      } else if (state == AppIdentify.IDENT_ON) {
        resState.setValue("On");
      } else {
        warn(TAG, "Invalid identify state for " + address.getHostAddress());
      }
    }
  }

  @Override
  public void onAppIdentifyGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber) {
    if (MessageDebugHelpers.isMessageForMe(TAG, this.endpoint, endpoint, status)) {
      resVersion.setValue(String.valueOf(version));
    }
  }

  @Override
  public void onAppIdentifySetIdentifyResponse(InetAddress address, byte endpoint, int status,
      byte sequenceNumber) {
    MessageDebugHelpers.isMessageForMeAndValidStatus(TAG, this.endpoint, endpoint, status);
  }

  @Override
  public void onDeviceMessage(Message msg) {
    if (MessageDebugHelpers.isSameAddresses(TAG, this.address, msg.getAddress())) {
      msg.callHandler(this);
    }
  }

  @Override
  public void onDeviceMessageTimeout(int msgId) {
    MessageDebugHelpers.debugOnMessageTimeout(TAG, address, msgId);
  }

  @Override
  public void remoteSetAsync(String path, List<String> parameters) {
    MessageDebugHelpers.setResource(TAG, path, parameters, root);
  }

  @Override
  public void remoteUpdateAll() {
    root.remoteGetAll();
  }

}
