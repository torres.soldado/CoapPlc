package org.keeper.iot.wsn.plc.messages.appidentify;

import org.keeper.iot.wsn.plc.messages.DataTypes;

import java.net.InetAddress;

public interface AppIdentify {
  public static final byte IDENT_CMD_GET_VERSION = 0;
  public static final byte IDENT_CMD_IDENTIFY = 1;
  public static final byte IDENT_CMD_GET_STATE = 2;

  public static final byte IDENT_ATT_STATE = 0;
  public static final byte IDENT_ATT_STATE_DATATYPE = DataTypes.DATATYPE_UINT8_ID;
  public static final byte IDENT_ATT_STATE_LENGTH = 1;
  public static final byte IDENT_ATT_TIMEOUT = 1;
  public static final byte IDENT_ATT_TIMEOUT_DATATYPE = DataTypes.DATATYPE_UINT16_ID;
  public static final byte IDENT_ATT_TIMEOUT_LENGTH = 2;

  public static final byte IDENT_OFF = 0x00;
  public static final byte IDENT_ON = 0x01;

  /**
   * Response with cluster version if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param endpoint Endpoint that originated this message.
   * @param status PLC status code bit-fields.
   * @param version Version of this cluster.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppIdentifyGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber);

  public void onAppIdentifyGetStateResponse(InetAddress address, byte endpoint, int status,
      byte state, byte sequenceNumber);

  public void onAppIdentifySetIdentifyResponse(InetAddress address, byte endpoint, int status,
      byte sequenceNumber);
}
