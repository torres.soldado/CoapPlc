/**
 * Creates a response {@link Message} from raw data.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appsmarttemp;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppSmartTempGetMinTempResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppSmartTempGetMinTempResponse.class.getSimpleName();

  /**
   * Used to register observer against this message Id.
   * 
   * @param endpoint Endpoint.
   * @return Registration id of a AppGenericsGetVersionResponse..
   */
  public static final int getRegistrationId(byte endpoint) {
    return MessageFields.createId(new byte[] {0, 0, MessageFields.SOF,
        MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_APP, MessageFields.APP_CLUSTER_SMARTTEMP,
        AppSmarttemp.ST_CMD_GET_MIN_TEMP, endpoint, 0, 0});
  }

  /**
   * The PLC status bit-fields.
   */
  private final int status;

  /**
   * The temperature value.
   */
  private final byte temperature;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public AppSmartTempGetMinTempResponse(InetAddress address, byte[] data)
      throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 7);
    this.temperature = data[11];
  }

  /**
   * Don't allow.
   */
  private AppSmartTempGetMinTempResponse() {
    super(null, null);
    this.status = 0;
    this.temperature = 0;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof AppSmarttemp) {
      ((AppSmarttemp) obs).onAppSmarttempGetMinTempResponse(super.address,
          MessageFields.getEndpoint(super.data), this.status, this.temperature,
          MessageFields.getSequenceNumber(data));
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return LOG_TAG + ": "
        + MessageDebugHelpers.printEndpoint(super.data)
        + MessageDebugHelpers.printStatus(this.status) 
        + " MinTemperature(" + Helpers.byteToHexString(this.temperature) + "°C) "
        + MessageDebugHelpers.printSequenceNumber(super.data)
        + super.toString();
  }
}
