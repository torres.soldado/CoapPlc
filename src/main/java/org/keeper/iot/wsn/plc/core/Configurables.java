package org.keeper.iot.wsn.plc.core;

/**
 * Configurable parameters for Plc.
 */
public class Configurables {
  public static final int PLC_ENDPOINT_PORT = 5000;
  public static final int PLC_SERVER_PORT = 5001;
  public static int DEFAULT_RESPONSE_TIMEOUT_MILLISECONDS = 3000;
}
