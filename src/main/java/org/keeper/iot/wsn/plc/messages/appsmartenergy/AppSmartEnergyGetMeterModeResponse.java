/**
 * Creates a response {@link Message} from raw data.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appsmartenergy;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.MessageStatus;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppSmartEnergyGetMeterModeResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppSmartEnergyGetMeterModeResponse.class.getSimpleName();

  /**
   * Used to register observer against this message Id.
   * 
   * @param endpoint Endpoint.
   * @return Registration id of a AppGenericsGetVersionResponse..
   */
  public static final int getRegistrationId(byte endpoint) {
    return MessageFields.createId(new byte[] {0, 0, MessageFields.SOF,
        MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_APP,
        MessageFields.APP_CLUSTER_SMARTENERGY, AppSmartEnergyCommands.SE_CMD_GET_OP_MODE, endpoint,
        0, 0});
  }

  /**
   * The plc status bitfields.
   */
  private final int status;
  /**
   * The meter operation mode.
   */
  private final byte opMode;

  /**
   * The sequence number of this message.
   */
  private final byte sequenceNumber;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public AppSmartEnergyGetMeterModeResponse(InetAddress address, byte[] data)
      throws IllegalArgumentException {
    super(address, data);
    this.opMode = data[7];
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 8);
    this.sequenceNumber = MessageFields.getSequenceNumber(data);
  }

  /**
   * Don't allow.
   */
  private AppSmartEnergyGetMeterModeResponse() {
    super(null, null);
    this.status = 0;
    this.opMode = 0;
    this.sequenceNumber = 0;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof AppSmartEnergy) {
      ((AppSmartEnergy) obs).onAppSmartEnergyGetMeterModeResponse(super.address,
          MessageFields.getEndpoint(super.data), this.status, this.opMode, this.sequenceNumber);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return LOG_TAG + ": " + super.toString() + " endpoint("
        + Integer.toString(MessageFields.getEndpoint(super.data)) + "), Status("
        + MessageStatus.getStatusStr(this.status) + "), opMode("
        + Helpers.byteToHexString(this.opMode) + "), sequenceNumber("
        + Integer.toString(this.sequenceNumber) + ")";
  }
}
