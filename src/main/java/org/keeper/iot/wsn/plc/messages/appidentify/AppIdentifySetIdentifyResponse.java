/**
 * Creates a response {@link Message} from raw data.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appidentify;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppIdentifySetIdentifyResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppIdentifySetIdentifyResponse.class.getSimpleName();

  /**
   * Used to register observer against this message Id.
   * 
   * @param endpoint Endpoint.
   * @return Registration id of a AppGenericsGetVersionResponse..
   */
  public static final int getRegistrationId(byte endpoint) {
    return MessageFields.createId(new byte[] {0, 0, MessageFields.SOF,
        MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_APP, MessageFields.APP_CLUSTER_IDENTIFY,
        AppIdentify.IDENT_CMD_IDENTIFY, endpoint, 0, 0});
  }

  /**
   * The PLC status bit fields.
   */
  private final int status;

  /**
   * The sequence number of this message.
   */
  private final byte sequenceNumber;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public AppIdentifySetIdentifyResponse(InetAddress address, byte[] data)
      throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 7);
    this.sequenceNumber = MessageFields.getSequenceNumber(data);
  }

  /**
   * Don't allow.
   */
  private AppIdentifySetIdentifyResponse() {
    super(null, null);
    this.status = 0;
    this.sequenceNumber = 0;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof AppIdentify) {
      ((AppIdentify) obs).onAppIdentifySetIdentifyResponse(super.address,
          MessageFields.getEndpoint(super.data), this.status, this.sequenceNumber);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /**
   * Description of message.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  @Override
  public String toString() {
    return LOG_TAG + ": "
        + super.toString()
        + MessageDebugHelpers.printCmdClusterId(super.data)
        + MessageDebugHelpers.printEndpoint(super.data)
        + MessageDebugHelpers.printStatus(this.status)
        + MessageDebugHelpers.printSequenceNumber(super.data)
        + super.toString();
  }
}
