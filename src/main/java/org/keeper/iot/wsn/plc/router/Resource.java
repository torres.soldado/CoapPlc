package org.keeper.iot.wsn.plc.router;

import static com.esotericsoftware.minlog.Log.debug;
import static com.esotericsoftware.minlog.Log.trace;
import static com.esotericsoftware.minlog.Log.warn;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Holds a queue of messages to be sent. A message is handled to the router and sent if the
 * previous message has received a response or times out.
 * Controls timeout events and informs router when timeout occurs.
 */
public class Resource {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = Resource.class.getSimpleName();

  /**
   * Informs router when timeout occurs. Should be cancelled when response arrives.
   * 
   * @author keeper
   */
  private class TimeoutTask extends TimerTask {
    /**
     * Method run.
     * 
     * 
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
      synchronized (messagesToSend) {
        trace(LOG_TAG, "Timeout, discarding message:" + messagesToSend.getFirst().toString());
        router.onTimeout(messagesToSend.getFirst());
        messagesToSend.removeFirst();
        if (!messagesToSend.isEmpty()) {
          router.send(messagesToSend.getFirst());
          reschedTimer(messagesToSend.getFirst().getResponseTimeout());
        }
      }
    }
  }

  /**
   * Messages awaiting to be dispatched.
   */
  private final LinkedList<Message> messagesToSend = new LinkedList<>();
  /**
   * To send messages through.
   */
  private final IRouter router;

  /**
   * Counts time since message was sent. If it times out executes task to inform observer of
   * timeout.
   */
  private Timer timer = new Timer(LOG_TAG, true);

  /**
   * Constructor for Resource.
   * 
   * @param router IRouter
   */
  public Resource(IRouter router) {
    this.router = router;
  }

  /**
   * Constructor for Resource.
   */
  @SuppressWarnings("unused")
  private Resource() {
    this.router = null;
  }

  /**
   * Method add.
   * Add a message to message queue. It will be dispatched when there is no message awaiting a
   * response.
   * 
   * @param message Message
   */
  public void add(Message message) {
    trace(LOG_TAG, "Enqueueing message:" + MessageDebugHelpers.printMessageId(message.getId()));

    final boolean wasEmpty;
    synchronized (messagesToSend) {
      wasEmpty = messagesToSend.isEmpty();
      messagesToSend.addLast(message);
    }

    if (wasEmpty) {
      router.send(message);
      reschedTimer(message.getResponseTimeout());
    }
  }

  /**
   * Re-sets the timer.
   */
  private void reschedTimer(int milliseconds) {
    timer = new Timer(LOG_TAG, true); // TODO can you avoid creating a new obj?
    timer.schedule(new TimeoutTask(), milliseconds);
  }

  /**
   * Method notifyArrival.
   * Someone calls this to let this know a message with the given id has arrived.
   * 
   * @param messageId int
   */
  public void notifyArrival(int messageId) {
    synchronized (messagesToSend) {
      if (!messagesToSend.isEmpty()) {
        trace(
            LOG_TAG,
            "Message with " + MessageDebugHelpers.printMessageId(messageId)
                + "arrived, currently waiting for "
                + MessageDebugHelpers.printMessageId(messagesToSend.getFirst().getResponseId()));
        if (messagesToSend.getFirst().getResponseId() == messageId) {
          timer.cancel();
          debug(LOG_TAG, "Expected response received, releasing entry");
          messagesToSend.removeFirst();
          if (!messagesToSend.isEmpty()) {
            router.send(messagesToSend.getFirst());
            reschedTimer(messagesToSend.getFirst().getResponseTimeout());
          }
        } else {
          warn(LOG_TAG, "Unexpected response received");
        }
      } else {
        warn(LOG_TAG, "Not expecting a message at all.");
      }
    }
  }
}
