package org.keeper.iot.wsn.plc.messages;

import static com.esotericsoftware.minlog.Log.*;

import org.keeper.iot.wsn.plc.resources.PlcResource;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.util.List;

public class MessageDebugHelpers {
  /**
   * Gets sequence number textual representation.
   * 
   * @param data Raw message data.
   * @return Textual representation of sequence number.
   */
  public static String printSequenceNumber(byte[] data) {
    return "sequenceNumber(" + Integer.toString((int) (MessageFields.getSequenceNumber(data)) & 0xFF)
        + ") ";
  }

  /**
   * Gets cmdClusterId textual representation.
   * 
   * @param data Raw message data.
   * @return Textual representation of cmdClusterId.
   */
  public static String printCmdClusterId(byte[] data) {
    return "cmdClusterId(" + Integer.toString(MessageFields.getClusterCmdId(data) & 0xFF) + ") ";
  }

  /**
   * Gets endpont textual representation.
   * 
   * @param data Raw message data.
   * @return Textual representation of endpoint.
   */
  public static String printEndpoint(byte[] data) {
    return "endpoint(" + MessageFields.getEndpoint(data) + ") ";
  }

  /**
   * Gets cmd0 byte from message data and returns a textual representing.
   * 
   * @param data Message raw data.
   * @return Textual representation of cmd0.
   */
  public static String printCmd0(byte[] data) {
    final byte cmd0 = MessageFields.getCmd0(data);
    final byte type = MessageFields.getType(cmd0);
    final byte system = MessageFields.getSystem(cmd0);
    return "cmd0(0x" + Helpers.byteToHexString(cmd0) + ", " + MessageFields.getCmdTypeStr(type)
        + ", " + MessageFields.getSystemStr(system) + ") ";
  }

  /**
   * Gets cmd1 textual representation.
   * 
   * @param data Raw message data.
   * @return Textual representation of cmd1.
   */
  public static String printCmd1(byte[] data) {
    return "cmd1(0x" + Helpers.byteToHexString(MessageFields.getCmd1AkaClusterId(data)) + ") ";
  }

  /**
   * Gets length textual representation.
   * 
   * @param data Raw message data.
   * @return Textual representation of length.
   */
  public static String printLength(int length) {
    return "length(" + Integer.toString(length) + ") ";
  }

  /**
   * Gets checksum textual representation.
   * 
   * @param data Raw message data.
   * @return Textual representation of checksum.
   */
  public static String printChecksum(byte checksum) {
    return "checksum(" + Helpers.byteToHexString(checksum) + ") ";
  }

  /**
   * Gets message id textual representation.
   * 
   * @param data Raw message data.
   * @return Textual representation of message id.
   */
  public static String printMessageId(int id) {
    return "id(0x" + Helpers.intToHexString(id) + ") ";
  }

  /**
   * Gets raw data textual representation.
   * 
   * @param data Raw message data.
   * @return Textual representation of raw data.
   */
  public static String printRawData(byte[] data, int size) {
    return "data:\n" + Helpers.arrayToHexStringPretty(data, size);
  }

  /**
   * Gets raw data textual representation.
   * 
   * @param msg Message.
   * @return Textual representation of raw data.
   */
  public static String printRawData(Message msg) {
    return "data:\n" + Helpers.arrayToHexStringPretty(msg.getRaw(), msg.getLength() + 2);
  }

  /**
   * Gets status textual representation.
   * 
   * @param data Raw message data.
   * @return Textual representation of status.
   */
  public static String printStatus(int status) {
    return "status(" + MessageStatus.getStatusStr(status) + ") ";
  }

  /**
   * Print helper.
   * 
   * @param tag Instance tag for logger.
   */
  public static void incompatibleHandler(String tag) {
    warn(tag, "Incompatible handler");
  }

  /**
   * Compare two InetAddresses.
   */
  public static boolean isSameAddresses(String tag, InetAddress expected, InetAddress actual) {
    if (actual == expected) {
      return true;
    }

    boolean areSame = Helpers.compareArrays(expected.getAddress(), actual.getAddress());
    if (!areSame) {
      warn(
          tag,
          "onDeviceMessage Observer got message from wrong address: should get "
              + expected.getHostAddress() + " but got " + actual.getHostAddress());
    }
    return areSame;
  }

  public static void debugOnMessageTimeout(String tag, InetAddress address, int messageId) {
    warn(tag, "Message timeout for " + address.getHostAddress() + " of message with id: 0x"
        + Helpers.intToHexString(messageId));
  }

  public static boolean isMessageForMeAndValidStatus(String tag, int expectedEndpoint, int actualEndpoint,
      int status) {
    if (!isMessageForMe(tag, expectedEndpoint, actualEndpoint, status)) {
      trace(tag, "Error in message:" + MessageStatus.getStatusStr(status));
      return false;
    }
    
    return true;
  }
  
  public static boolean isMessageForMe(String tag, int expectedEndpoint, int actualEndpoint,
      int status) {
    if (actualEndpoint != expectedEndpoint) {
      error(tag, "Ednpoints don't match.");
      return false;
    }

    return MessageStatus.isStatusSuccessfull(tag, status);
  }

  public static boolean isAttributeExpected(String tag, int expectedClusterId, int actualClusterId,
      int expectedAttributeId, int actualAttributeId) {
    if (actualClusterId == expectedClusterId && actualAttributeId == expectedAttributeId) {
      return true;
    }

    error(tag, "Attribute doesn't match");
    return false;
  }

  public static void onUnhandledMessage(String tag, String methodName) {
    error(tag, "Unexpected message called unhandled at" + methodName);
  }

  public static void setResource(String tag, String path, List<String> parameters, PlcResource root) {
    PlcResource resource = root.getChild(path.split("\\\\"), 0);
    if (null != resource) {
      resource.remoteSet(parameters);
    } else {
      error(tag, "Unknown resource with URI " + path);
    }
  }
}
