/**
 * NwkAnnounceRequest.java
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.nwk;

import static com.esotericsoftware.minlog.Log.warn;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;

import java.net.InetAddress;

/**
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class NwkAnnounceDeviceRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = NwkAnnounceDeviceRequest.class.getSimpleName();
  /**
   * Used to register observer against this message Id.
   */
  public static final int REGISTRATION_ID = MessageFields
      .createId(new byte[] {0, 0, MessageFields.SOF,
          MessageFields.CMDO_AREQ | MessageFields.SUBSYSTEM_NWK, Nwk.NWK_ANNOUNCE, 0});

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   */
  public NwkAnnounceDeviceRequest(InetAddress address, byte[] data) {
    super(address, data);
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof Nwk) {
      ((Nwk) obs).onNwkAnnounceRequest(super.getAddress());
    } else {
      warn(LOG_TAG, "Attempt to call non-existing handler");
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  public String toString() {
    return LOG_TAG + ": " + super.toString();
  }

}
