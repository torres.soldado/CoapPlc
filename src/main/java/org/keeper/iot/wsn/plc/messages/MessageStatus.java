package org.keeper.iot.wsn.plc.messages;

import static com.esotericsoftware.minlog.Log.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class MessageStatus {
  
  
  /**
   * Status bit fields.
   */
  public static final int STATUS_BAD_ATTRIBUTE_DATA = 0x00020000;
  /**
   * Field STATUS_BAD_ATTRIBUTE_DATATYPE.
   */
  public static final int STATUS_BAD_ATTRIBUTE_DATATYPE = 0x00008000;
  /**
   * Field STATUS_BAD_ATTRIBUTE_LEN.
   */
  public static final int STATUS_BAD_ATTRIBUTE_LEN = 0x00010000;
  /**
   * Field STATUS_BAD_CMD_ARG.
   */
  public static final int STATUS_BAD_CMD_ARG = 0x00000800;
  /**
   * Field STATUS_BAD_PACKET_CHECKSUM.
   */
  public static final int STATUS_BAD_PACKET_CHECKSUM = 0x00004000;
  /**
   * Field STATUS_BAD_PACKET_LEN.
   */
  public static final int STATUS_BAD_PACKET_LEN = 0x00002000;
  /**
   * Field STATUS_BAD_PACKET_SOF.
   */
  public static final int STATUS_BAD_PACKET_SOF = 0x00001000;
  /**
   * Field STATUS_DEVICE_MISMATCH.
   */
  public static final int STATUS_DEVICE_MISMATCH = 0x00000004;
  /**
   * Field STATUS_ERROR_ATTRIBUTE_OP.
   */
  public static final int STATUS_ERROR_ATTRIBUTE_OP = 0x00100000;
  /**
   * Field STATUS_ERROR_BUSY.
   */
  public static final int STATUS_ERROR_BUSY = 0x00080000;
  /**
   * Field STATUS_ERROR_LOCAL_OVERRIDE.
   */
  public static final int STATUS_ERROR_LOCAL_OVERRIDE = 0x00200000;
  /**
   * Field STATUS_FILE_ERROR.
   */
  public static final int STATUS_FILE_ERROR = 0x02000000;
  /**
   * Field STATUS_NULL_POINTER.
   */
  public static final int STATUS_NULL_POINTER = 0x00000010;
  /**
   * Field STATUS_OK.
   */
  public static final int STATUS_OK = 0x00000001;
  /**
   * Field STATUS_SLOTS_UNAVAILABLE.
   */
  public static final int STATUS_SLOTS_UNAVAILABLE = 0x00040000;
  /**
   * Field STATUS_SOCKET_SENDING_ERROR.
   */
  public static final int STATUS_SOCKET_SENDING_ERROR = 0x00400000;
  /**
   * Field STATUS_TIMEOUT.
   */
  public static final int STATUS_TIMEOUT = 0x01000000;
  /**
   * Field STATUS_TOO_MANY_RETRIES.
   */
  public static final int STATUS_TOO_MANY_RETRIES = 0x00800000;
  /**
   * Field STATUS_UNDEFINED_ERROR.
   */
  public static final int STATUS_UNDEFINED_ERROR = 0x00000008;
  /**
   * Field STATUS_UNKNOWN_DEVICE.
   */
  public static final int STATUS_UNKNOWN_DEVICE = 0x00000002;
  /**
   * Field STATUS_UNSUPPORTED_ATTRIB_OP.
   */
  public static final int STATUS_UNSUPPORTED_ATTRIB_OP = 0x00000400;
  /**
   * Field STATUS_UNSUPPORTED_ATTRIBUTE.
   */
  public static final int STATUS_UNSUPPORTED_ATTRIBUTE = 0x00000200;
  /**
   * Field STATUS_UNSUPPORTED_CLUSTER.
   */
  public static final int STATUS_UNSUPPORTED_CLUSTER = 0x00000080;
  /**
   * Field STATUS_UNSUPPORTED_COMMAND.
   */
  public static final int STATUS_UNSUPPORTED_COMMAND = 0x00000100;
  /**
   * Field STATUS_UNSUPPORTED_ENDPOINT.
   */
  public static final int STATUS_UNSUPPORTED_ENDPOINT = 0x00000020;
  /**
   * Field STATUS_UNSUPPORTED_SUBSYSTEM.
   */
  public static final int STATUS_UNSUPPORTED_SUBSYSTEM = 0x00000040;

  /**
   * Field statusMap.
   */
  private static final Map<Integer, String> STATUS_MAP;

  static {
    final Map<Integer, String> map = new HashMap<Integer,String>(32, 1.0f);
    map.put(0, "STATUS_OK");
    map.put(1, "STATUS_UNKNOWN_DEVICE");
    map.put(2, "STATUS_DEVICE_MISMATCH");
    map.put(3, "STATUS_UNDEFINED_ERROR");
    map.put(4, "STATUS_NULL_POINTER");
    map.put(5, "STATUS_UNSUPPORTED_ENDPOINT");
    map.put(6, "STATUS_UNSUPPORTED_SUBSYSTEM");
    map.put(7, "STATUS_UNSUPPORTED_CLUSTER");
    map.put(8, "STATUS_UNSUPPORTED_COMMAND");
    map.put(9, "STATUS_UNSUPPORTED_ATTRIBUTE");
    map.put(10, "STATUS_UNSUPPORTED_ATTRIB_OP");
    map.put(11, "STATUS_BAD_CMD_ARG");
    map.put(12, "STATUS_BAD_PACKET_SOF");
    map.put(13, "STATUS_BAD_PACKET_LEN");
    map.put(14, "STATUS_BAD_PACKET_CHECKSUM");
    map.put(15, "STATUS_BAD_ATTRIBUTE_DATATYPE");
    map.put(16, "STATUS_BAD_ATTRIBUTE_LEN");
    map.put(17, "STATUS_BAD_ATTRIBUTE_DATA");
    map.put(18, "STATUS_SLOTS_UNAVAILABLE");
    map.put(19, "STATUS_ERROR_BUSY");
    map.put(20, "STATUS_ERROR_ATTRIBUTE_OP");
    map.put(21, "STATUS_ERROR_LOCAL_OVERRIDE");
    map.put(22, "STATUS_SOCKET_SENDING_ERROR");
    map.put(23, "STATUS_TOO_MANY_RETRIES");
    map.put(24, "STATUS_TIMEOUT");
    map.put(25, "STATUS_FILE_ERROR");

    STATUS_MAP = Collections.unmodifiableMap(map);
  }

  /**
   * Given the value of the status field in a message, returns it's textual representation.
   * 
   * @param status Status value extracted from a message.
   * 
   * 
   * @return String with textual representation of status field.
   */
  public static String getStatusStr(int status) {
    final StringBuilder sb = new StringBuilder(2048);
    for (int i = 0; i < 32; ++i) {
      if (0 != (status & (1 << i))) {
        sb.append(STATUS_MAP.get(Integer.valueOf(i)) + " ");
      }
    }

    return sb.toString();
  }
  
  public static boolean isStatusSuccessfull(String tag, int status) {
    boolean successfull = (status & STATUS_OK) != 0;
    if (!successfull) {
      warn(tag, "Message has bad status.");
    }
    return successfull;
  }
}
