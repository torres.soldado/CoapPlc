/**
 * Helpers class.
 */

package org.keeper.utils;

import static com.esotericsoftware.minlog.Log.*;
import java.util.Random;

/**
 * Helper functions for general use.
 * 
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public final class Helpers {
  /**
   * All hexadecimal characters.
   */
  private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

  /**
   * No instantiation allowed.
   */
  private Helpers() {}

  /**
   * Return text representation.
   * 
   * @return String representing object.
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "";
  }

  /**
   * Compare two byte arrays.
   * 
   * @param expected Byte array one.
   * @param actual Byte array two.
   */
  public static boolean compareArrays(byte[] expected, byte[] actual) {
    if (expected == actual) {
      return true;
    }

    if (expected == null || actual == null) {
      trace("compareArrays", "Can't be null");
      return false; // Don't consider null instances.
    }

    if (expected.length != actual.length) {
      trace("compareArrays", "Length mismatch");
      return false;
    }

    for (int i = 0; i < actual.length; ++i) {
      if (actual[i] != expected[i]) {
        trace("compareArrays",
            "Byte #" + i + " mismatch, expecting 0x" + Integer.toString(expected[i] & 0xFF)
                + ", got 0x" + Integer.toString(actual[i] & 0xFF));
        return false;
      }
    }

    return true;
  }

  /**
   * Copy 16bit integer value to an array in little endian format.
   * 
   * @param val16 Integer value to be copied from.
   * @param dst Destination array.
   * @param startIdx Destination array offset.
   */
  public static void setInt16InArrayLittleEndian(int val16, byte[] dst, int startIdx) {
    dst[startIdx] = (byte) (val16 & 0xFF);
    dst[startIdx + 1] = (byte) ((val16 >> 8) & 0xFF);
  }

  /**
   * Copy 32bit integer value to an array in little endian format.
   * 
   * @param val32 Integer value to be copied from.
   * @param dst Destination array.
   * @param startIdx Destination array offset.
   */
  public static void setInt32InArrayLittleEndian(int val32, byte[] dst, int startIdx) {
    dst[startIdx] = (byte) (val32 & 0xFF);
    dst[startIdx + 1] = (byte) ((val32 >> 8) & 0xFF);
    dst[startIdx + 2] = (byte) ((val32 >> 16) & 0xFF);
    dst[startIdx + 3] = (byte) ((val32 >> 24) & 0xFF);
  }

  /**
   * Copy 64bit integer value to an array in little endian format.
   * 
   * @param val32 Integer value to be copied from.
   * @param dst Destination array.
   * @param startIdx Destination array offset.
   */
  public static void setInt64InArrayLittleEndian(int val32, byte[] dst, int startIdx) {
    dst[startIdx] = (byte) (val32 & 0xFF);
    dst[startIdx + 1] = (byte) ((val32 >> 8) & 0xFF);
    dst[startIdx + 2] = (byte) ((val32 >> 16) & 0xFF);
    dst[startIdx + 3] = (byte) ((val32 >> 24) & 0xFF);
    dst[startIdx + 4] = (byte) ((val32 >> 32) & 0xFF);
    dst[startIdx + 5] = (byte) ((val32 >> 40) & 0xFF);
    dst[startIdx + 6] = (byte) ((val32 >> 48) & 0xFF);
    dst[startIdx + 7] = (byte) ((val32 >> 56) & 0xFF);
  }

  /**
   * Get 16bit unsigned value from byte array in little endian.
   * 
   * @param src byte[] Source array to extract integer, must contain at least 2 bytes.
   * @param startIdx int Offset in the array.
   * @return int Resulting integer.
   */
  public static int getUnsigned16FromArrayLittleEndian(byte[] src, int startIdx) {
    return (src[startIdx] & 0xFF) | ((src[startIdx + 1] & 0xFF) << 8);
  }

  /**
   * Get 32bit unsigned value from byte array in little endian.
   * 
   * @param src byte[] Source array to extract integer, must contain at least 4 bytes.
   * @param startIdx int Offset in the array.
   * @return int Resulting integer.
   */
  public static long getUnsigned32FromArrayLittleEndian(byte[] src, int startIdx) {
    return ((src[startIdx] & 0xFF) | ((src[startIdx + 1] & 0xFF) << 8)
        | ((src[startIdx + 2] & 0xFF) << 16) | ((src[startIdx + 3] & 0xFF) << 24)) & 0x00FFFFFFFFL;
  }

  /**
   * Get 64bit unsigned value from byte array in little endian.
   * 
   * @param src byte[] Source array to extract integer, must contain at least 8 bytes.
   * @param startIdx int Offset in the array.
   * @return long Resulting value.
   */
  public static long getUnsigned64FromArrayLittleEndian(byte[] src, int startIdx) {
    return (long) (src[startIdx] & 0xFF) | ((long) (src[startIdx + 1] & 0xFF) << 8)
        | ((long) (src[startIdx + 2] & 0xFF) << 16) | ((long) (src[startIdx + 3] & 0xFF) << 24)
        | ((long) (src[startIdx + 4] & 0xFF) << 32) | ((long) (src[startIdx + 5] & 0xFF) << 40)
        | ((long) (src[startIdx + 6] & 0xFF) << 48) | ((long) (src[startIdx + 7] & 0xFF) << 56);
  }

  /**
   * Get 16bit signed value from byte array in little endian.
   * 
   * @param src byte[] Source array to extract integer, must contain at least 2 bytes.
   * @param startIdx int Offset in the array.
   * @return int Resulting integer.
   */
  public static int getSigned16FromArrayLittleEndian(byte[] src, int startIdx) {
    int i;
    if (0 != (src[startIdx + 1] & 0x80)) {
      // negative number
      i = -1;
      i &= ~0x7FFF;
      i |= (src[startIdx] & 0xFF) | ((src[startIdx + 1] & 0xFF) << 8);
    } else {
      i = (src[startIdx] & 0xFF) | ((src[startIdx + 1] & 0xFF) << 8);
    }
    return i;
  }

  /**
   * Get 32bit signed value from byte array in little endian.
   * 
   * @param src byte[] Source array to extract integer, must contain at least 4 bytes.
   * @param startIdx int Offset in the array.
   * @return int Resulting integer.
   */
  public static int getSigned32FromArrayLittleEndian(byte[] src, int startIdx) {
    int i;
    if (0 != (src[startIdx + 3] & 0x80)) {
      // negative number
      i = -1;
      i &= ~0x7FFFFFFF;
      i |=
          (src[startIdx] & 0xFF) | ((src[startIdx + 1] & 0xFF) << 8)
              | ((src[startIdx + 2] & 0xFF) << 16) | ((src[startIdx + 3] & 0xFF) << 24);
    } else {
      i =
          (src[startIdx] & 0xFF) | ((src[startIdx + 1] & 0xFF) << 8)
              | ((src[startIdx + 2] & 0xFF) << 16) | ((src[startIdx + 3] & 0xFF) << 24);
    }

    return i;
  }

  /**
   * Get 64bit signed value from byte array in little endian.
   * 
   * @param src byte[] Source array to extract integer, must contain at least 4 bytes.
   * @param startIdx int Offset in the array.
   * @return int Resulting integer.
   */
  public static long getSigned64FromArrayLittleEndian(byte[] src, int startIdx) {
    long i;
    if (0 != (src[startIdx + 3] & 0x80)) {
      // negative number
      i = -1;
      i &= 0x8000000000000000L;
      i |=
          (src[startIdx] & 0xFF) | ((src[startIdx + 1] & 0xFF) << 8)
              | ((src[startIdx + 2] & 0xFF) << 16) | ((src[startIdx + 3] & 0xFF) << 24)
              | ((src[startIdx + 4] & 0xFF) << 32) | ((src[startIdx + 5] & 0xFF) << 40)
              | ((src[startIdx + 6] & 0xFF) << 48) | ((src[startIdx + 7] & 0xFF) << 56);
    } else {
      i =
          (src[startIdx] & 0xFF) | ((src[startIdx + 1] & 0xFF) << 8)
              | ((src[startIdx + 2] & 0xFF) << 16) | ((src[startIdx + 3] & 0xFF) << 24)
              | ((src[startIdx + 4] & 0xFF) << 32) | ((src[startIdx + 5] & 0xFF) << 40)
              | ((src[startIdx + 6] & 0xFF) << 48) | ((src[startIdx + 7] & 0xFF) << 56);
    }

    return i;
  }

  /**
   * Method toHexString.
   * 
   * @param val byte Byte to convert to ASCII hex.
   * @return String String Resulting ASCII hex string.
   */
  public static String byteToHexString(byte val) {
    return new String(new char[] {HEX_ARRAY[(val >> 4) & 0x0F], HEX_ARRAY[val & 0x0F]});
  }

  /**
   * Converts integer to hexadecimal string.
   * 
   * @param val 32bit integer value to convert.
   * @return String Resulting ASCII hex string.
   */
  public static String intToHexString(int val) {
    return new String(new char[] {HEX_ARRAY[(val >> 28) & 0x0F], HEX_ARRAY[(val >> 24) & 0x0F],
        HEX_ARRAY[(val >> 20) & 0x0F], HEX_ARRAY[(val >> 16) & 0x0F],
        HEX_ARRAY[(val >> 12) & 0x0F], HEX_ARRAY[(val >> 8) & 0x0F], HEX_ARRAY[(val >> 4) & 0x0F],
        HEX_ARRAY[val & 0x0F]});
  }

  /**
   * Converts something like 0x123456 into "123456".
   * 
   * @param bytes byte[] Byte array to be converted to ASCII hex.
   * @param count int Number of bytes of the array to convert.
   * @return String Resulting ASCII hex string.
   */
  public static String arrayToHexString(byte[] bytes, int count) {
    final char[] hexChars = new char[count << 1];
    int val;
    for (int j = 0; j < count; j++) {
      val = bytes[j] & 0xFF;
      hexChars[j << 1] = HEX_ARRAY[val >> 4];
      hexChars[(j << 1) + 1] = HEX_ARRAY[val & 0x0F];
    }
    return new String(hexChars);
  }

  /**
   * Prints something like: 0000: 00 00 00 00 0001: 00 00 00 01 0002: 00 00 00 02 ...
   * 
   * @param bytes Array of bytes to convert to ASCII coded hex.
   * @param count int Integer to convert to ASCII hex.
   * @return String Resulting ASCII hex string.
   */
  public static String arrayToHexStringPretty(byte[] bytes, int count) {
    final int nBytesPerRow = 8;
    final int nCols = 6 + nBytesPerRow * 3;
    int lastRow = count / nBytesPerRow;
    final int lastCol = count % nBytesPerRow;
    if (0 != lastCol) {
      lastRow++;
    }

    final char[] hexChars = new char[lastRow * nCols];

    int offset = 0;
    int offsetBytes;
    int limC;
    for (int row = 0; row < lastRow; ++row) {
      offset = row * nCols;
      offsetBytes = row * nBytesPerRow;

      hexChars[offset++] = HEX_ARRAY[(row & 0xF000) >> 12];
      hexChars[offset++] = HEX_ARRAY[(row & 0x0F00) >> 8];
      hexChars[offset++] = HEX_ARRAY[(row & 0x00F0) >> 4];
      hexChars[offset++] = HEX_ARRAY[(row & 0x000F)];
      hexChars[offset++] = ':';

      limC = nBytesPerRow;
      if ((row == lastRow - 1) && (0 != lastCol)) {
        limC = lastCol;
      }

      for (int c = 0; c < limC; ++c) {
        hexChars[offset++] = ' ';
        hexChars[offset++] = HEX_ARRAY[(bytes[offsetBytes] >> 4) & 0x0F];
        hexChars[offset++] = HEX_ARRAY[bytes[offsetBytes++] & 0x0F];
      }
      hexChars[offset++] = '\n';
    }

    hexChars[offset - 1] = '\0';
    return new String(hexChars);
  }

  /**
   * Converts a hexadecimal string byte representation into a byte array.
   * 
   * @param str String in ASCII hex to convert to array of bytes.
   * @return byte[] Byte array with the hexadecimal values.
   */
  public static byte[] hexStringToByteArray(String str) {
    final int len = str.length();
    final byte[] data = new byte[len >> 1];
    for (int i = 0; i < len; i += 2) {
      data[i >> 1] =
          (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character
              .digit(str.charAt(i + 1), 16));
    }
    return data;
  }

  /**
   * Copies from one byte array to another. The destination array should be able to accomodate data.
   * 
   * @param src Source byte array.
   * @param dst Destination byte array.
   * @param srcStartIndex Offset from source byte array.
   * @param numberOfBytes Number of bytes to copy.
   * @param dstStartIndex Destination array offset.
   */
  public static void arrayToArray(byte[] src, byte[] dst, int srcStartIndex, int numberOfBytes,
      int dstStartIndex) {
    for (int i = 0; i < numberOfBytes; ++i) {
      dst[dstStartIndex + i] = src[srcStartIndex + i];
    }
  }

  /**
   * Returns a pseudo-random number between min and max, inclusive. The difference between min and
   * max can be at most <code>Integer.MAX_VALUE - 1</code>.
   *
   * @param minVal Minimum value
   * @param maxVal Maximum value. Must be greater than min.
   * @return Integer between min and max, inclusive.
   * @see java.util.Random#nextInt(int)
   */
  public static int getRandomInt(int minVal, int maxVal) {

    // NOTE: Usually this should be a field rather than a method
    // variable so that it is not re-seeded every call.
    final Random rand = new Random();

    // nextInt is normally exclusive of the top value,
    // so add 1 to make it inclusive
    final int randomNum = rand.nextInt((maxVal - minVal) + 1) + minVal;

    return randomNum;
  }
}
