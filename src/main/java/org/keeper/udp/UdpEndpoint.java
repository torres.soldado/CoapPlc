/**
 * UdpEndpoint class.
 */

package org.keeper.udp;

import static com.esotericsoftware.minlog.Log.error;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Whenever a packet is read it is handled to the listener.
 * Also allows sending a datagram to the network.
 * 
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class UdpEndpoint implements IDatagramSender {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = UdpEndpoint.class.getSimpleName();

  /**
   * Thread that runs the udpListener.
   */
  private final Thread listenerThread;

  /**
   * The socket we listen to.
   */
  private final DatagramSocket socket;

  /**
   * Waits until packet arrives.
   */
  private final UdpCapture udpListener;

  /**
   * Return text representation.
   * 
   * @return String representing object.
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "";
  }

  /**
   * 
   * @param udpPacketHandler A handler to which any received datagram will be forwarded to.
   * @param portToListenTo The udp port to listen to.
   * 
   * @throws SocketException If the socket can't be created.
   */
  public UdpEndpoint(IDatagramListener udpPacketHandler, int portToListenTo) throws SocketException {
    try {
      this.socket = new DatagramSocket(portToListenTo);
      this.udpListener = new UdpCapture(this.socket, udpPacketHandler);
      this.listenerThread = new Thread(this.udpListener, "Udp listener thread");
    } catch (SocketException e) {
      error(LOG_TAG, "could not create socket");
      throw e;
    }
  }


  /**
   * Unused.
   */
  @SuppressWarnings("unused")
  private UdpEndpoint() {
    socket = null;
    udpListener = null;
    listenerThread = null;
  }

  /**
   * Method start.
   * Starts a thread that listens for datagrams.
   */
  public IDatagramSender start() {
    if (!this.listenerThread.isAlive()) {
      this.listenerThread.start();
    }
    return this;
  }

  /**
   * Method sendPacket.
   * 
   * @param packet DatagramPacket
   * 
   * @see org.keeper.udp.IDatagramSender#sendPacket(DatagramPacket)
   */
  @Override
  public void sendPacket(DatagramPacket packet) {
    try {
      this.socket.send(packet);
    } catch (IOException e) {
      error(LOG_TAG, "failed to send packet: " + e.getMessage());
    }
  }

  /**
   * Method stop.
   * 
   * 
   * @throws InterruptedException If it fails to stop endpoint gracefully.
   */
  public void stop() throws InterruptedException {
    this.udpListener.stop();
    this.listenerThread.join();
  }

}
