/**
 * Udp capture class.
 */

package org.keeper.udp;

import static com.esotericsoftware.minlog.Log.error;
import static com.esotericsoftware.minlog.Log.trace;

import org.keeper.iot.wsn.plc.messages.MessageFields;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;

/**
 * Listens on a socket and when a packet is received performs callback.
 * 
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class UdpCapture implements Runnable {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = UdpCapture.class.getSimpleName();
  /**
   * True if thread is running.
   */
  private boolean isRunning;
  /**
   * The socket.
   */
  private final DatagramSocket socket;
  /**
   * A listener to which we deliver captured datagrams.
   */
  private final IDatagramListener handler;

  /**
   * @param socket Socket instance to listen to.
   * @param handler Handler that datagrams will be delivered to.
   */
  public UdpCapture(DatagramSocket socket, IDatagramListener handler) {
    if (null == socket) {
      error(LOG_TAG, "socket can't be null");
      throw new IllegalArgumentException(ServerSocket.class.getCanonicalName() + " cannot be null.");
    }

    this.isRunning = true;
    this.socket = socket;
    this.handler = handler;
  }

  /**
   * Don't allow.
   */
  @SuppressWarnings("unused")
  private UdpCapture() {
    socket = null;
    handler = null;
  }

  /**
   * Return text representation.
   * 
   * @return String representing object.
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "";
  }

  /**
   * Method run.
   * 
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    byte[] rxPayload = new byte[MessageFields.MAX_MESSAGE_LENGTH];
    DatagramPacket packet = new DatagramPacket(rxPayload, rxPayload.length);
    while (this.isRunning) {
      try {
        this.socket.receive(packet);
        this.handler.onDatagram(packet); // deliver packet to udpHandler
      } catch (IOException e) {
        error(LOG_TAG, "failure receiving packet:" + e.getMessage());
        this.isRunning = false;
      } finally {
        if (!this.isRunning) {
          this.socket.close();
        }
      }
    }
  }

  /**
   * Method clone.
   * 
   * @return Object
   * @throws CloneNotSupportedException Does this always.
   */
  @Override
  public Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException("Not supported");
  }

  /**
   * Stops thread.
   */
  public void stop() {
    trace(LOG_TAG, "stopping");
    this.isRunning = false;
    if (!this.socket.isClosed()) {
      this.socket.close();
    }
  }
}
